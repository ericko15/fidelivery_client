(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('ImprimirPedidoService', ImprimirPedidoService);

    ImprimirPedidoService.$inject = ['$mdDialog', 'ImpresionService'];
    function ImprimirPedidoService($mdDialog, ImpresionService) {
        var vm = {},
            service = { init: init },
            user = JSON.parse(localStorage.getItem('user'))

        return service;

        function init(scope) {
            vm = scope
            scope.imprimirPedido = imprimirPedido
            scope.closeDialog = closeDialog
            scope.getFechaHoraActual = getFechaHoraActual
            scope.imprimirTicket = imprimirTicket
            scope.getTotalProducto = getTotalProducto
            scope.imprimir = false
            return scope
        }

        function imprimirPedido(ev, pedido) {
            vm.pedido = pedido
            $mdDialog.show({
                contentElement: '#myStaticDialog',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            }).then(function (data) {
                if (data) {
                    vm.imprimir = true
                    ImpresionService.imprimir({
                        user: user,
                        pedido: pedido,
                        mesa: 1,
                        mesero: 1
                    });
                    vm.imprimir = false
                }
            })
        }

        function getTotalProducto(producto) {
            return producto.pivot.quantity * producto.pivot.price
        }

        function closeDialog() {
            $mdDialog.hide()
        }

        function imprimirTicket() {
            vm.imprimir = true
            $mdDialog.hide(true)
        }

        function getFechaHoraActual() { return new Date(); }
    }
})();
