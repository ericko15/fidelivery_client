(function () {
  'use strict';

  angular
    .module('app.core')
    .factory('CalculoValoresService', CalculoValoresService);

  CalculoValoresService.$inject = [];
  function CalculoValoresService() {

    var service = {
      getSubtotalPedido: getSubtotalPedido,
      getTotalPedido: getTotalPedido,
      getTotalProducto: getTotalProducto,
      getValorIVA: getValorIVA,
      getValorPedidos: getValorPedidos,
      getValorSinIVA: getValorSinIVA
    };

    return service;

    ///////////////

    function getSubtotalPedido(pedido) {
      var subtotal = 0;
      pedido.productos.forEach(function (element) {
        var totalProducto = getTotalProducto(element) || 0;
        subtotal += parseInt(totalProducto);
      }, this);
      return subtotal;
    }

    function getTotalPedido(pedido) {
      var subtotal = getSubtotalPedido(pedido) || 0;
      var valorDomicilio = pedido.valor_domicilio || 0;
      return subtotal + valorDomicilio;
    }

    function getTotalProducto(item) {
      var valor = item.valor || 0;
      var cantidad = item.pivot.cantidad || 1;
      return valor * cantidad;
    }

    function getValorIVA(valor) { return valor - getValorSinIVA(valor); }

    function getValorPedidos(pedidos) {
      return sumarElementosArray(getPropertyInArrayObject(pedidos, 'subtotal')) +
        sumarElementosArray(
          getPropertyInArrayObject(pedidos, 'valor_domicilio', 'tipo_mensajero', 'propio')
        );
    }

    function getValorSinIVA(valor) { return valor / 1.19; }
  }
})();
