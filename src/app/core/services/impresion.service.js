(function () {
    'use strict';

    angular.module('app.core')
        .factory('ImpresionService', ImpresionService);

    ImpresionService.$inject = ['$filter', 'CalculoValoresService'];
    function ImpresionService($filter, CalculoValoresService) {
        var service = {
            imprimir: imprimir,
        };
        return service;
        function getStringImpresionBluetooth(user, pedido, mesa, mesero) {
            var printingString = '$bigh$' + user.vendedor.sede.establecimiento.nombre + '$intro$';
            if (pedido.esComanda) {
                printingString += '$bigh$Comanda$intro$';
            } else {
                printingString += '$small$NIT: ' + user.vendedor.sede.establecimiento.nit + '$intro$';
                printingString += '$small$' + user.vendedor.sede.nombre +
                    ': ' + user.vendedor.sede.direccion + ', ' +
                    user.vendedor.sede.barrio + '$intro$';
                printingString += '$small$Contacto: ' + user.vendedor.sede.numero + '$intro$';
                printingString += '$small$' + user.vendedor.sede.ciudad + ', ' +
                    user.vendedor.sede.departamento + '$intro$';
            }
            // Si el pedido no tiene id, se asigna el ultimo_consecutivo de la sede + 1.
            var consecutivo = !pedido.id ? user.ultimo_consecutivo + 1 : pedido.consecutivo;
            // Si el pedido no ha sido despachado, 
            // se asigna la fecha actual en formato yyyy-MM-dd HH:mm:ss.
            var fechaPedido = pedido.enviado ? pedido.updated_at : $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss');
            printingString += '$small$# ' + consecutivo +
                (pedido.esComanda && pedido.id ? ' (ModificaciÃ³n)' : '') +
                ' | Fecha: ' + fechaPedido + '$intro$';
            printingString += '$small$------------------------------------------$intro$';
            printingString += '$big$Detalles del pedido$intro$';
            printingString += '$small$------------------------------------------$intro$';
            pedido.productos.forEach(function (producto) {
                printingString += '$bigw$' + producto.pivot.cantidad +
                    ' | ' + producto.nombre + ' ' + (producto.pivot.comentario || '') +
                    (producto.es_agregado ? ' (AGREGADO)' : '') + '$intro$';

                var valorUnidad = getStringValorMoneda(producto.valor);
                printingString += '$big$Valor unidad: ' + valorUnidad + '$intro$';

                var totalProducto = getStringValorMoneda(CalculoValoresService.getTotalProducto(producto));
                printingString += 'Total producto: ' + totalProducto + '$intro$';
                printingString += '$small$------------------------------------------$intro$';
            });

            var subtotal = getStringValorMoneda(pedido.subtotal);
            var valorDomicilio = pedido.valor_domicilio ?
                getStringValorMoneda(pedido.valor_domicilio) : 'No registrado';
            printingString += pedido.tipo_pedido == 'domicilio' ?
                '$big$Subtotal: ' + subtotal + '$intro$' : '';
            printingString += pedido.tipo_pedido == 'domicilio' ?
                '$big$Valor domicilio: ' + valorDomicilio + '$intro$' : '';

            var muestraIva = user.vendedor.sede.establecimiento.registra_iva && !pedido.esComanda;

            var total = getStringValorMoneda(pedido.total);
            printingString += '$big$Total: ' + total +
                (muestraIva ? ' (IVA incluido)' : '') + '$intro$';

            var valorIVA = getStringValorMoneda(CalculoValoresService.getValorIVA(pedido.total), undefined, 2);
            printingString += muestraIva ? '$big$IVA: ' + valorIVA + '$intro$' : '';
            printingString += !pedido.esComanda && pedido.tipo_pedido == 'mesa' ?
                '$small$La propina es voluntaria.$intro$' : '';
            printingString += '$small$------------------------------------------$intro$';

            if (pedido.cliente && pedido.cliente.nombre_completo) {
                printingString += '$big$Cliente: ' + pedido.cliente.nombre_completo + '$intro$';
                printingString += '$big$Contacto: ' + pedido.numero + '$intro$';
            }
            if (pedido.tipo_pedido == 'mesa') {
                printingString += '$big$Para la mesa: ' + mesa.numero + '$intro$';
                printingString += '$big$Mesero: ' + mesero.primer_nombre +
                    ' ' + mesero.primer_apellido + '$intro$';
            }
            printingString += pedido.tipo_pedido == 'domicilio' ?
                '$big$DirecciÃ³n: ' + pedido.direccion + '$intro$' : '';
            printingString += '$small$Pedido realizdo con$intro$';
            printingString += '$small$Fidelivery, Â¡MÃ¡s cerca de tus clientes!$intro$';
            printingString += '$small$ContÃ¡ctanos: 313 519 5944$intro$';
            printingString += '$small$http://www.fidelivery.co$intro$';
            printingString += '$intro$$intro$$intro$';
            return printingString;
        }

        /**
         * Convierte el espacio creado por el $filter('currency') 'Â '
         * a un espacio normal ' ' 
         * y retorna el valor string de la moneda con el espacio normal.
         * Es decir: 'Â ' => ' ' El primer espacio es diferente al segundo.
         * @param {number} valor El valor a convertir en formato de moneda.
         * @param {string} [simbolo] El sÃ­mbolo de la moneda. Por defecto: '$'
         * @param {number} [decimales] Cantidad de decimales para mostrar en el valor de la moneda. Por defecto: 0
         * @returns {string}
         */
        function getStringValorMoneda(valor, simbolo, decimales) {
            valor = valor || 0;
            simbolo = simbolo || '$';
            decimales = decimales || 0;
            return $filter('currency')(valor, simbolo, decimales).replace('Â ', ' ');
        }

        function imprimir(detalles) {
            var ua = navigator.userAgent.toLowerCase();
            var isAndroid = ua.indexOf("android") > -1;
            isAndroid ?
                imprimirPedidoAndroid(detalles.user, detalles.pedido, detalles.mesa, detalles.mesero) :
                imprimirPedidoNavegador('resumen-pedido');
        }

        function imprimirPedidoAndroid(user, pedido, mesa, mesero) {
            var printingString = getStringImpresionBluetooth(user, pedido, mesa, mesero);
            var driver = "com.fidelier.printfromweb://";
            location.href = driver + printingString;
        }

        /**
         * Imprime a travÃ©s del navegador el pedido,
         * recibiendo como parÃ¡metro el id del contenedor (div) a imprimir.
         * @param {string} muestra El id del contenedor (div) a imprimir.
         */
        function imprimirPedidoNavegador(muestra) {
            var data = jQuery('#' + muestra).html();
            var myWindow = window.open('', '_blank');
            myWindow.document.write('<html><head><title>Pedido de Fidelivery</title>');
            myWindow.document.write('<link href="css/impresion-pedido.css" rel="stylesheet" />');
            myWindow.document.write(data);
            myWindow.document.write('</body></html>');
            setTimeout(function () {
                myWindow.print();
                myWindow.close();
            }, 125);
        }
    }
})();