(function () {
  'use strict';

  angular
    .module('app.toolbar')
    .controller('ToolbarController', ToolbarController);

  /** @ngInject */
  function ToolbarController($rootScope, $q, $state, $timeout, $mdSidenav, $translate, $mdToast,
    msNavigationService, $auth, $location, $scope, $window) {
    var vm = this;
    // Data
    $rootScope.global = { search: '' };
    vm.bodyEl = angular.element('body');
    // Methods
    vm.toggleSidenav = toggleSidenav;
    vm.logout = logout;
    vm.toggleHorizontalMobileMenu = toggleHorizontalMobileMenu;
    vm.toggleMsNavigationFolded = toggleMsNavigationFolded;
    //variables
    vm.user = JSON.parse(localStorage.getItem('user'));
    vm.nombre = vm.user.name.charAt(0).toUpperCase() + vm.user.name.slice(1);

    /**
     * Toggle sidenav
     *
     * @param sidenavId
     */
    function toggleSidenav(sidenavId) {
      $mdSidenav(sidenavId).toggle();
    }


    /**
     * Logout Function
     */
    function logout() {
      $auth.logout()
        .then(function () {
          var user = JSON.parse(sessionStorage.getItem("user"));
          message("Hasta pronto " + vm.nombre);
          sessionStorage.removeItem("user");
          sessionStorage.clear();
          $location.path("/");
        });
    }

    /**
     * message Function
     * Función para mostrar mensajes
     */
    function message(body) {
      $mdToast.show({
        template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + body + '</div></md-toast>',
        hideDelay: 3000,
        position: 'top right',
        parent: '#content'
      });
    }

    /**
     * Toggle horizontal mobile menu
     */
    function toggleHorizontalMobileMenu() {
      vm.bodyEl.toggleClass('ms-navigation-horizontal-mobile-menu-active');
    }

    /**
     * Toggle msNavigation folded
     */
    function toggleMsNavigationFolded() {
      msNavigationService.toggleFolded();
    }
  }

})();