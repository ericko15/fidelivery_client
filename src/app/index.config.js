(function () {
  'use strict'

  angular.module('fuse').config(config);

  /** @ngInject */
  function config($translateProvider, $mdDateLocaleProvider, api, RestangularProvider, $authProvider) {
    // Put your common app configurations here
    RestangularProvider.setBaseUrl(api);
    $authProvider.loginUrl = api + "auth/login";
    $authProvider.tokenName = "token";
    $authProvider.tokenPrefix = "fidelivery";
    $authProvider.tokenRoot = 'result';

    // angular-translate configuration
    $translateProvider.useLoader('$translatePartialLoader', {urlTemplate: '{part}/i18n/{lang}.json'});
    $translateProvider.preferredLanguage('en');
    $translateProvider.useSanitizeValueStrategy('sanitize');

    // Formato de fecha del md-datepicker.
    $mdDateLocaleProvider.formatDate = function (date) {
      return moment(date).format('DD-MM-YYYY')
    }
  }
})();
