(function () {
  'use strict';

  angular.module('app.pages.auth.login')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController($mdToast, $auth, Restangular, $state) {
    var vm = this;
    vm.form = {};
    vm.ingresar = login;

    /**
     * @description funcion para hacer login
     */
    function login() {
      $auth.login({
        username: vm.form.usuario,
        password: vm.form.password
      }).then(function (response) {
        if (response.data) {
          message('¡Bienvenido a Fidelivery!');
          var user = $auth.getPayload().user
          cargarProductos(user)
          localStorage.setItem('user', JSON.stringify(user))
          if (user.rol === 1) {
            localStorage.setItem('establecimientos', JSON.stringify(response.data.establishments))
            localStorage.setItem('administradores', JSON.stringify(response.data.administrators))
            $state.go('app.usuarios')
          } else if (user.rol === 2) {
            $state.go('app.historial-pedidos')
          } else if (user.rol === 3) {
            localStorage.setItem('meseros', JSON.stringify(response.data.waiters))
            localStorage.setItem('clientes', JSON.stringify(response.data.customers))
            $state.go('app.pedidos')
          }
        }
      }).catch(function (response) {
        if (response.data.message) {
          message(response.data.message);
        } else {
          message('Hubo un error, contacte a soporte');
        }
      });
    };

    /**
     * @description funcion para mostrar un mensaje en pantalla
     * @param {String} body
     */
    function message(body) {
      $mdToast.show({
        template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + body + '</div></md-toast>',
        hideDelay: 3000,
        position: 'top right',
        parent: '#content'
      });
    }

    /**
     * @description Funcion para cargar los productos de una sede
     * @param {Object} user 
     */
    function cargarProductos(user) {
      if (user.rol == 3) {
        var establecimiento = user.employee.headquarter.establishment_id,
          sede = user.employee.headquarter_id
        Restangular.one('establishments', establecimiento)
          .one('headquarters', sede)
          .all('products')
          .getList({})
          .then(function (response) {
            localStorage.setItem('productos', JSON.stringify(response))
          })
      }
    }
  }
})();
