(function () {
  'use strict';

  angular
    .module('app.administrador.mesas', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    // State
    $stateProvider
      .state('app.mesas', {
        url: '/mesas',
        views: {
          'content@app': {
            templateUrl: 'app/main/administrador/mesas/mesas.html',
            controller: 'MesasController as vm'
          }
        },
        bodyClass: 'administrador',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isAdmin: _isAdmin
        }
      });
  }
})();
