(function () {
  'use strict';

  angular
    .module('app.administrador.mesas.nueva-mesa')
    .controller('NuevaMesaController', NuevaMesaController);

  /** @ngInject */
  function NuevaMesaController($auth, Restangular, $mdToast, $state) {
    var vm = this,
      user = $auth.getPayload().user;
    vm.mesa = {};
    vm.establecimientos = user.administrator.establishments;
    vm.cargarSedes = cargarSedes
    vm.guardar = guardar

    /**
     * Funcion para guardar la mesa
     */
    function guardar() {
      Restangular.one('establishments', vm.establecimiento)
        .one('headquarters', vm.sede)
        .all('tables')
        .post(vm.mesa)
        .then(function (data) {
          message("Creado");
          $state.go('app.mesas')
        })
        .catch(function (response){
          if (response.status === 422) {
            if (response.data && response.data.number[0] == 'The number has already been taken.') {
              message("El numero de la mesa ya ha sido tomado");
            }
          }
        })
    };

    /**
     * Funcion para mostrar un mensaje en pantalla
     * @param {any} body 
     */
    function message(body) {
      $mdToast.show({
        template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + body + '</div></md-toast>',
        hideDelay: 3000,
        position: 'top right',
        parent: '#content'
      });
    }
   
    /**
     * Funcion para cargar las sedes dependiendo del establecimiento seleccionado
    */
    function cargarSedes() {
      vm.sedes = vm.establecimientos.find(function (data) {
        if (data.id === vm.establecimiento) {
          return data
        }
      }).headquarters
      vm.sede = vm.sedes[0].id
    };
  }
})();
