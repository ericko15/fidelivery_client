(function () {
  'use strict';

  angular
    .module('app.administrador.mesas.nueva-mesa', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('app.nueva-mesa', {
        url: '/mesas/nueva-mesa',
        views: {
          'content@app': {
            templateUrl: '/app/main/administrador/mesas/nueva-mesa/nueva-mesa.html',
            controller: 'NuevaMesaController as vm'
          }
        },
        bodyClass: 'administrador',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isAdmin: _isAdmin
        }
      });
  }

})();
