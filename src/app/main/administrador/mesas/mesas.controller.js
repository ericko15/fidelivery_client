(function () {
  'use strict';

  angular
    .module('app.administrador.mesas')
    .controller('MesasController', MesasController);

  /** @ngInject */
  function MesasController($scope, $mdDialog, $auth, Restangular) {
    var vm = this;
    vm.usuario = $auth.getPayload().user;
    vm.establecimientos = vm.usuario.administrator.establishments
    vm.mesas = [];
    vm.dtInstance = {};
    vm.dtOptions = {
      dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
      columnDefs: [{
        // Target the actions column
        targets: 3,
        responsivePriority: 1,
        filterable: false,
        sortable: false
      }],
      initComplete: function () {
        var api = this.api(),
          searchBox = angular.element('body').find('#administrador-meseros-search');
        // Bind an external input as a table wide search box
        if (searchBox.length > 0) {
          searchBox.on('keyup', function (event) {
            api.search(event.target.value).draw()
          })
        }
      },
      autoWidth: false,
      language: {
        sEmptyTable: "No hay mesas registradas",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
        sInfoFiltered: "(filtrado desde _MAX_ registrsos)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLengthMenu: "Mostrar _MENU_ registros",
        sLoadingRecords: "Cargando...",
        sProcessing: "Procesando...",
        sSearch: "Buscar:",
        sZeroRecords: "No se encontraron registros que coincidar con la busqueda",
        oPaginate: {
          sFirst: "Primero",
          sLast: "Último",
          sNext: "Siguiente",
          sPrevious: "Anterior"
        },
        oAria: {
          sSortAscending: ": activar para ordenar las columnas ascendente",
          sSortDescending: ": activar para ordenar las columnas descendente"
        }
      },
      pagingType: 'simple',
      lengthMenu: [10, 20, 30, 50, 100],
      pageLength: 20,
      responsive: true
    }
    vm.showDialogModificar = showDialogModificar
    vm.showDialogEliminar = showDialogEliminar
    vm.seleccionarEstablecimiento = seleccionarEstablecimiento
    vm.cargarMesas = cargarMesas

    /**
     * @description Funcion para cargar todos los valores iniciales
     * @returns {void}
     */
    function init() {
      vm.establecimiento = vm.establecimientos[0].id;
      seleccionarEstablecimiento()
    }

    /**
     * @description Funcion para cargar los meseros
     * @returns {void}
     */
    function cargarMesas() {
      Restangular.one('establishments', vm.establecimiento)
        .one('headquarters', vm.sede)
        .all('tables')
        .getList({})
        .then(function (data) {
          vm.mesas = data
        })
    }

    /**
     * @description Funcion para mostrar la pantalla de modificar mesero
     * @param {any} ev 
     * @param {any} data 
     * @returns {void}
     */
    function showDialogModificar(ev, data) {
      $mdDialog.show({
        controller: function ($scope, $mdToast, $mdDialog, datos, Restangular) {
          $scope.mesa = datos
          $scope.establecimiento = datos.headquarter.establishment_id
          $scope.sede = datos.headquarter.id
          $scope.closeDialog = function () {
            $mdDialog.hide()
          }
          $scope.update = function () {
            Restangular.one('establishments', $scope.establecimiento)
              .one('headquarters', $scope.sede)
              .one('tables', datos.id)
              .patch($scope.mesa)
              .then(function (data) {
                $mdDialog.hide(data)
              }).catch(function (response) {
                if (response.status === 422) {
                  if (response.data && response.data.number[0] == 'The number has already been taken.') {
                    $mdToast.show({
                      template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">El numero de la mesa ya ha sido tomado</div></md-toast>',
                      hideDelay: 3000,
                      position: 'top right',
                      parent: '#content'
                    });
                  }
                }
              })
          }
        },
        templateUrl: 'app/main/administrador/mesas/dialog-modificar-mesa-template.html',
        targetEvent: ev,
        locals: {
          datos: data
        },
        clickOutsideToClose: true
      }).then(function (data) {
        if (data) {
          cargarMesas()
        }
      })

    };

    /**
     * @description Funcion para mostrar la pantalla de eliminar mesero
     * @returns {void}
     * @param {any} ev 
     * @param {any} data 
     */
    function showDialogEliminar(ev, data) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, datos, user) {
          $scope.number = datos.number
          $scope.closeDialog = function () {
            $mdDialog.hide()
          };
          $scope.remove = function () {
            Restangular.one('establishments', vm.establecimiento)
              .one('headquarters', vm.sede)
              .one('tables', datos.id)
              .remove()
              .then(function (data) {
                $mdDialog.hide(data)
              })
          }
        },
        templateUrl: 'app/main/administrador/mesas/dialog-eliminar-mesa-template .html',
        targetEvent: ev,
        locals: {
          datos: data,
          user: $auth.getPayload().user
        },
        clickOutsideToClose: true
      }).then(function (data) {
        if (data) {
          cargarMesas()
        }
      })
    };

    /**
     * @description Funcion para cargar las sedes dependiendo el establecimiento
     * @returns {void}
     */
    function seleccionarEstablecimiento() {
      vm.sedes = []
      if (vm.establecimiento > 0) {
        vm.sedes = vm.usuario.administrator.establishments.find(function (data) {
          if (data.id === vm.establecimiento) {
            return data
          }
        }).headquarters;
        vm.sede = vm.sedes[0].id
        vm.cargarMesas()
      }
    }

    init()
  }
})();
