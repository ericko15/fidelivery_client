(function () {
  'use strict';

  angular
    .module('app.administrador', [
      'app.administrador.informacion-cuenta',
      'app.administrador.historial-pedidos',
      'app.administrador.reporte-tablas',
      'app.administrador.reporte-graficas',
      'app.administrador.meseros',
      'app.administrador.meseros.nuevo-mesero',
      'app.administrador.mesas',
      'app.administrador.mesas.nueva-mesa',
      'app.administrador.productos',
      'app.administrador.productos.nuevo-producto',
      'app.administrador.clientes'
    ]).config(config);

  /** @ngInject */
  function config(msNavigationServiceProvider) {

    msNavigationServiceProvider.saveItem('historial-pedidos', {
      title: 'Historial de pedidos',
      icon: 'icon-clipboard-text',
      state: 'app.historial-pedidos',
      bodyClass: 'administrador',
      rol: 'ADMIN'
    });

    msNavigationServiceProvider.saveItem('reporte-graficas', {
      title: 'Reporte general en gráficas',
      icon: 'icon-chart-line',
      state: 'app.reporte-graficas',
      bodyClass: 'administrador',
      rol: 'ADMIN'
    });

    msNavigationServiceProvider.saveItem('informacion-cuenta', {
      title: 'Información de la cuenta',
      icon: 'icon-account',
      state: 'app.informacion-cuenta',
      bodyClass: 'administrador',
      rol: 'ADMIN'
    });

    msNavigationServiceProvider.saveItem('reporte-tablas', {
      title: 'Reporte general en tablas',
      icon: 'icon-chart-bar',
      state: 'app.reporte-tablas',
      bodyClass: 'administrador',
      rol: 'ADMIN'
    });

    msNavigationServiceProvider.saveItem('meseros', {
      title: 'Gestionar meseros',
      icon: 'icon-account-multiple',
      state: 'app.meseros',
      bodyClass: 'administrador',
      rol: 'ADMIN'
    });

    msNavigationServiceProvider.saveItem('mesas', {
      title: 'Gestionar mesas',
      icon: 'icon-chair-school',
      state: 'app.mesas',
      bodyClass: 'administrador',
      rol: 'ADMIN'
    });

    msNavigationServiceProvider.saveItem('productos', {
      title: 'Gestionar Productos',
      icon: 'icon-food',
      state: 'app.productos',
      bodyClass: 'administrador',
      rol: 'ADMIN'
    });

    msNavigationServiceProvider.saveItem('clientes', {
      title: 'Ver clientes',
      icon: 'icon-eye',
      state: 'app.clientes',
      bodyClass: 'administrador',
      rol: 'ADMIN'
    });
  }
})();
