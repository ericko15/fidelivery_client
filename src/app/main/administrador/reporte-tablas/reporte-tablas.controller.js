(function () {
  'use strict';

  angular
    .module('app.administrador.reporte-tablas')
    .controller('ReporteTablasController', ReporteTablasController);

  /** @ngInject */
  function ReporteTablasController(Restangular, $auth, $mdToast, $filter) {
    var vm = this,
    user = $auth.getPayload().user;
    vm.establecimientos = user.administrator.establishments;
    vm.seleccionarEstablecimiento = seleccionarEstablecimiento;
    vm.cargarPedidosPorDia = cargarPedidosPorDia;
    vm.getTotalPedidosDiaLapso = getTotalPedidosDiaLapso;
    vm.getTotalValorPorDia = getTotalValorPorDia;


    function init() {
      cargarPedidosPorDia();
    }

    init();

    function seleccionarEstablecimiento() {
      if (vm.establecimiento != null) {
        vm.sedes = vm.establecimientos.find(function (data) {
          return data.id === vm.establecimiento
        }).headquarters
        vm.sede = null
      }
      cargarPedidosPorDia();
    }

    function cargarPedidosPorDia() {
      var initialDate = (vm.initialDate) ? formato_fecha(vm.initialDate) : formato_fecha(new Date())
      var finalDate = (vm.finalDate) ? formato_fecha(vm.initialDate) : formato_fecha(new Date())
      var establecimientoId = (!vm.establecimiento) ? null : vm.establecimiento
      var sedeId = (!vm.sede) ? null : vm.sede
      Restangular.all('administrators').all('orders')
        .getList({
          days: true,
          establishment_id: establecimientoId,
          headquarter_id: sedeId,
          initialDate: initialDate,
          finalDate: finalDate
        }).then(function (data) {
        vm.pedidos = [];
        vm.pedidos = data;
      })
        .catch(function (error) {
          var mensaje = (!error.status) ? error :
            String.format('Error: {0} {1}', error.status, error.statusText);
          message(mensaje);
        })
    }

    function getTotalPedidosDiaLapso() {
      return sumarElementosArray(getPropertyInArrayObject(vm.pedidos, 'dispatched'));
    }

    function getTotalValorPorDia() {
      return sumarElementosArray(getPropertyInArrayObject(vm.pedidos, 'total'));
    }

    function message(body) {
      $mdToast.show({
        template: '<md-toast id="' + body + '" layout="column" layout-align="center start">' +
        '<div class="md-toast-content">' + body + '</div></md-toast>',
        hideDelay: 5000,
        position: 'top right',
        parent: '#content'
      });
    }

    vm.dtInstance = {};
    vm.dtOptions = {
      dom: 'rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
      autoWidth: false,
      language: {
        sEmptyTable: "No hay registro despachado en la fecha seleccionada",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
        sInfoFiltered: "(filtrado desde _MAX_ registrsos)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLengthMenu: "Mostrar _MENU_ registros",
        sLoadingRecords: "Cargando...",
        sProcessing: "Procesando...",
        sSearch: "Buscar:",
        sZeroRecords: "No se encontraron registros que coincidar con la busqueda",
        oPaginate: {
          sFirst: "Primero",
          sLast: "Último",
          sNext: "Siguiente",
          sPrevious: "Anterior"
        },
        oAria: {
          sSortAscending: ": activar para ordenar las columnas ascendente",
          sSortDescending: ": activar para ordenar las columnas descendente"
        }
      },
      pagingType: 'simple',
      responsive: true
    };

  }

})();
