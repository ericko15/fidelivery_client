(function () {
  'use strict';

  angular
    .module('app.administrador.reporte-tablas', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('app.reporte-tablas', {
        url: '/reporte-tablas',
        views: {
          'content@app': {
            templateUrl: 'app/main/administrador/reporte-tablas/reporte-tablas.html',
            controller: 'ReporteTablasController as vm'
          }
        },
        bodyClass: 'administrador',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isAdmin: _isAdmin
        }
      });
  }
})();
