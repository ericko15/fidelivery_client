(function () {
  'use strict';

  angular
    .module('app.administrador.productos', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    // State
    $stateProvider
      .state('app.productos', {
        url: '/productos',
        views: {
          'content@app': {
            templateUrl: 'app/main/administrador/productos/productos.html',
            controller: 'ProductosController as vm'
          }
        },
        bodyClass: 'administrador',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isAdmin: _isAdmin
        }
      });
  }
})();
