(function () {
  'use strict';

  angular
    .module('app.administrador.productos')
    .controller('ProductosController', ProductosController);

  /** @ngInject */
  function ProductosController($scope, $mdDialog, $auth, Restangular) {
    var vm = this;
    vm.usuario = $auth.getPayload().user;
    vm.establecimientos = vm.usuario.administrator.establishments
    vm.productos = [];
    vm.dtInstance = {};
    vm.dtOptions = {
      dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
      columnDefs: [{
        responsivePriority: 1,
        filterable: false,
        sortable: false
      }],
      autoWidth: false,
      language: {
        sEmptyTable: "No hay productos registrados",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
        sInfoFiltered: "(filtrado desde _MAX_ registrsos)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLengthMenu: "Mostrar _MENU_ registros",
        sLoadingRecords: "Cargando...",
        sProcessing: "Procesando...",
        sSearch: "Buscar:",
        sZeroRecords: "No se encontraron registros que coincidar con la busqueda",
        oPaginate: {
          sFirst: "Primero",
          sLast: "Último",
          sNext: "Siguiente",
          sPrevious: "Anterior"
        },
        oAria: {
          sSortAscending: ": activar para ordenar las columnas ascendente",
          sSortDescending: ": activar para ordenar las columnas descendente"
        }
      },
      pagingType: 'simple',
      lengthMenu: [10, 20, 30, 50, 100],
      pageLength: 10,
      responsive: true
    }
    vm.showDialogModificar = showDialogModificar
    vm.showDialogEliminar = showDialogEliminar
    vm.seleccionarEstablecimiento = seleccionarEstablecimiento
    vm.cargarMesas = cargarMesas

    /**
     * Funcion para cargar todos los valores iniciales
     */
    function init() {
      vm.establecimiento = vm.establecimientos[0].id;
      seleccionarEstablecimiento()
    }

    /**
     * Funcion para cargar los meseros
     */
    function cargarMesas() {
      Restangular.one('establishments', vm.establecimiento)
        .one('headquarters', vm.sede)
        .all('products')
        .getList({})
        .then(function (data) {
          vm.productos = data
        })
    }

    /**
     * Funcion para mostrar la pantalla de modificar mesero
     * @param {any} ev 
     * @param {any} data 
     */
    function showDialogModificar(ev, data) {
      $mdDialog.show({
        controller: modificarController,
        templateUrl: 'app/main/administrador/productos/dialog-modificar-producto-template.html',
        targetEvent: ev,
        locals: {
          datos: data,
          categorias: vm.categorias
        },
        clickOutsideToClose: true
      }).then(function (data) {
        if (data) {
          cargarMesas()
        }
      })

    };

    /**
     * Funcion para mostrar la pantalla de eliminar mesero
     * @param {any} ev 
     * @param {any} data 
     */
    function showDialogEliminar(ev, data) {
      $mdDialog.show({
        controller: eliminarController,
        templateUrl: 'app/main/administrador/productos/dialog-eliminar-producto-template.html',
        targetEvent: ev,
        locals: {
          datos: data,
          user: $auth.getPayload().user
        },
        clickOutsideToClose: true
      }).then(function (data) {
        if (data) {
          cargarMesas()
        }
      })
    };

    /**
     * Funcion para cargar las sedes dependiendo el establecimiento
     */
    function seleccionarEstablecimiento() {
      vm.sedes = []
      if (vm.establecimiento > 0) {
        vm.sedes = vm.usuario.administrator.establishments.find(function (data) {
          if (data.id === vm.establecimiento) {
            return data
          }
        }).headquarters;
        vm.sede = vm.sedes[0].id
        vm.cargarMesas()
      }
    }

    /**
     * Función para cargar las categorias
     */
    function cargarCategorias() {
      Restangular.all('categories')
        .getList({})
        .then(function (data) {
          vm.categorias = data
        })
        .catch(function (response){
          message("Algo salió mal");
        })
    }

    /**
     * Funcion de controlador de la pantalla de modificar un producto
     */
    function modificarController ($scope, $mdToast, $mdDialog, datos, categorias, Restangular) {
      console.log(categorias)
      $scope.producto = datos
      $scope.categorias = categorias
      $scope.producto.picture = ($scope.producto.picture == null) ? '' : $scope.producto.picture
      $scope.producto.description = ($scope.producto.description == null) ? '' : $scope.producto.description
      $scope.establecimiento = datos.headquarter.establishment_id
      $scope.sede = datos.headquarter.id
      $scope.closeDialog = function () {
        $mdDialog.hide()
      }
      $scope.update = function () {
        Restangular.one('establishments', $scope.establecimiento)
          .one('headquarters', $scope.sede)
          .one('products', datos.id)
          .patch($scope.producto)
          .then(function (data) {
            $mdDialog.hide(data)
          })
      }
    }

    /**
     * Funcion de controlador de la pantalla de eliminar producto
     */
    function eliminarController($scope, $mdDialog, datos, user) {
      $scope.closeDialog = function () {
        $mdDialog.hide()
      };
      $scope.remove = function () {
        Restangular.one('establishments', vm.establecimiento)
          .one('headquarters', vm.sede)
          .one('products', datos.id)
          .remove()
          .then(function (data) {
            $mdDialog.hide(data)
          })
      }
    }

    cargarCategorias()
    init()
  }
})();
