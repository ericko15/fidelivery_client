(function () {
  'use strict';

  angular
    .module('app.administrador.productos.nuevo-producto')
    .controller('NuevoProductoController', NuevoProductoController);

  /** @ngInject */
  function NuevoProductoController($auth, Restangular, $mdToast, $state) {
    var vm = this,
      user = $auth.getPayload().user;
    vm.producto = {};
    vm.categorias = []
    vm.establecimientos = user.administrator.establishments;
    vm.cargarSedes = cargarSedes
    vm.guardar = guardar

    /**
     * Funcion para guardar el producto
     */
    function guardar() {
      Restangular.one('establishments', vm.establecimiento)
        .one('headquarters', vm.sede)
        .all('products')
        .post(vm.producto)
        .then(function (data) {
          message("Creado");
          $state.go('app.productos')
        })
        .catch(function (response){
          message("Algo salió mal");
        })
    };

    /**
     * Funcion para mostrar un mensaje en pantalla
     * @param {any} body 
     */
    function message(body) {
      $mdToast.show({
        template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + body + '</div></md-toast>',
        hideDelay: 3000,
        position: 'top right',
        parent: '#content'
      });
    }
   
    /**
     * Funcion para cargar las sedes dependiendo del establecimiento seleccionado
    */
    function cargarSedes() {
      vm.sedes = vm.establecimientos.find(function (data) {
        if (data.id === vm.establecimiento) {
          return data
        }
      }).headquarters
      vm.sede = vm.sedes[0].id
    };

    /**
     * Función para cargar las categorias
     */
    function cargarCategorias() {
      Restangular.all('categories')
        .getList({})
        .then(function (data) {
          vm.categorias = data
        })
        .catch(function (response){
          message("Algo salió mal");
        })
    }

    cargarCategorias()
  }
})();
