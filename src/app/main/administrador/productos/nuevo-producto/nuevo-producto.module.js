(function () {
  'use strict';

  angular
    .module('app.administrador.productos.nuevo-producto', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('app.nuevo-producto', {
        url: '/productos/nuevo-producto',
        views: {
          'content@app': {
            templateUrl: '/app/main/administrador/productos/nuevo-producto/nuevo-producto.html',
            controller: 'NuevoProductoController as vm'
          }
        },
        bodyClass: 'administrador',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isAdmin: _isAdmin
        }
      });
  }

})();
