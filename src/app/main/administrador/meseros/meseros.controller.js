(function () {
  'use strict';

  angular
    .module('app.administrador.meseros')
    .controller('MeserosController', MeserosController);

  /** @ngInject */
  function MeserosController($scope, $mdDialog, $auth, Restangular) {
    var vm = this;
    vm.usuario = $auth.getPayload().user;
    vm.establecimientos = vm.usuario.administrator.establishments
    vm.meseros = [];
    vm.dtInstance = {};
    vm.dtOptions = {
      dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
      columnDefs: [{
        responsivePriority: 1,
        filterable: false,
        sortable: false
      }],
      initComplete: function () {
        var api = this.api(),
          searchBox = angular.element('body').find('#administrador-meseros-search');
        // Bind an external input as a table wide search box
        if (searchBox.length > 0) {
          searchBox.on('keyup', function (event) {
            api.search(event.target.value).draw()
          })
        }
      },
      autoWidth: false,
      language: {
        sEmptyTable: "No hay meseros registrados",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
        sInfoFiltered: "(filtrado desde _MAX_ registrsos)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLengthMenu: "Mostrar _MENU_ registros",
        sLoadingRecords: "Cargando...",
        sProcessing: "Procesando...",
        sSearch: "Buscar:",
        sZeroRecords: "No se encontraron registros que coincidar con la busqueda",
        oPaginate: {
          sFirst: "Primero",
          sLast: "Último",
          sNext: "Siguiente",
          sPrevious: "Anterior"
        },
        oAria: {
          sSortAscending: ": activar para ordenar las columnas ascendente",
          sSortDescending: ": activar para ordenar las columnas descendente"
        }
      },
      pagingType: 'simple',
      lengthMenu: [10, 20, 30, 50, 100],
      pageLength: 10,
      responsive: true
    }
    vm.showDialogModificar = showDialogModificar
    vm.showDialogEliminar = showDialogEliminar
    vm.seleccionarEstablecimiento = seleccionarEstablecimiento
    vm.cargarMeseros = cargarMeseros

    /**
     * Funcion para cargar todos los valores iniciales
     */
    function init() {
      vm.establecimiento = vm.establecimientos[0].id;
      seleccionarEstablecimiento()
    }

    /**
     * Funcion para cargar los meseros
     */
    function cargarMeseros() {
      Restangular.one('establishments', vm.establecimiento)
        .one('headquarters', vm.sede)
        .all('employees')
        .getList({ type: 2 })
        .then(function (data) {
          console.log(data)
          vm.meseros = data
        })
    }

    /**
     * Funcion para mostrar la pantalla de modificar mesero
     * @param {any} ev 
     * @param {any} data 
     */
    function showDialogModificar(ev, data) {
      $mdDialog.show({
        controller: ModificarController,
        templateUrl: 'app/main/administrador/meseros/dialog-modificar-mesero-template.html',
        targetEvent: ev,
        locals: {
          datos: data,
          user: $auth.getPayload().user
        },
        clickOutsideToClose: true
      }).then(function (data) {
        if (data) {
          cargarMeseros()
        }
      })

    };

    /**
     * Funcion para mostrar la pantalla de eliminar mesero
     * @param {any} ev 
     * @param {any} data 
     */
    function showDialogEliminar(ev, data) {
      $mdDialog.show({
        controller: EliminarController,
        templateUrl: 'app/main/administrador/meseros/dialog-eliminar-mesero-template .html',
        targetEvent: ev,
        locals: {
          datos: data,
          user: $auth.getPayload().user
        },
        clickOutsideToClose: true
      }).then(function (data) {
        if (data) {
          cargarMeseros()
        }
      })
    };

    /**
     * Funcion controladora de la pantalla de modificar mesa
     */
    function ModificarController ($scope, $mdDialog, datos, user, Restangular) {
      if ((typeof datos.fecha_nacimiento) === 'string') {
        datos.fecha_nacimiento = datos.fecha_nacimiento.split('-')
        datos.fecha_nacimiento = new Date(datos.fecha_nacimiento[0], datos.fecha_nacimiento[1] - 1, datos.fecha_nacimiento[2])
      }
      delete datos.user.email
      $scope.mesero = datos.user
      $scope.mesero.document = datos.document
      $scope.establicimiento = datos.headquarter.establishment_id
      $scope.sede = datos.headquarter.id
      $scope.closeDialog = function () {
        $mdDialog.hide()
      }
      $scope.update = function () {
        Restangular.one('establishments', vm.establecimiento)
          .one('headquarters', vm.sede)
          .one('employees', datos.id)
          .patch($scope.mesero)
          .then(function (data) {
            $mdDialog.hide(data)
          })
      }
    }

    /**
     * Funcion controladora de la pantalla de eliminar mesa
     */
    function EliminarController ($scope, $mdDialog, datos, user) {
      $scope.mesero = datos
      $scope.closeDialog = function () {
        $mdDialog.hide()
      };
      $scope.remove = function () {
        Restangular.one('establishments', vm.establecimiento)
          .one('headquarters', vm.sede)
          .one('employees', datos.id)
          .remove()
          .then(function (data) {
            $mdDialog.hide(data)
          })
      }
    }

    /**
     * Funcion para cargar las sedes dependiendo el establecimiento
     */
    function seleccionarEstablecimiento() {
      vm.sedes = []
      if (vm.establecimiento > 0) {
        vm.sedes = vm.usuario.administrator.establishments.find(function (data) {
          if (data.id === vm.establecimiento) {
            return data
          }
        }).headquarters;
        vm.sede = vm.sedes[0].id;
        cargarMeseros()
      }
    }

    init()
  }
})();
