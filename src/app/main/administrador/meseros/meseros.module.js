(function () {
  'use strict';

  angular
    .module('app.administrador.meseros', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    // State
    $stateProvider
      .state('app.meseros', {
        url: '/meseros',
        views: {
          'content@app': {
            templateUrl: 'app/main/administrador/meseros/meseros.html',
            controller: 'MeserosController as vm'
          }
        },
        bodyClass: 'administrador',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isAdmin: _isAdmin
        }
      });
  }
})();
