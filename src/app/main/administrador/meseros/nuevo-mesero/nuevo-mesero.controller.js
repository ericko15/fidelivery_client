(function () {
  'use strict';

  angular
    .module('app.administrador.meseros.nuevo-mesero')
    .controller('NuevoMeseroController', NuevoMeseroController);

  /** @ngInject */
  function NuevoMeseroController($auth, Restangular, $mdToast, $state) {
    var vm = this,
      user = $auth.getPayload().user;
    vm.mesero = {};
    vm.establecimientos = user.administrator.establishments;
    vm.cargarSedes = cargarSedes
    vm.guardar = guardar

    /**
     * Funcion para guardar el mesero
     */
    function guardar() {
      vm.mesero.type = 2;
      Restangular.one('establishments', vm.establecimiento)
        .one('headquarters', vm.sede)
        .all('employees')
        .post(vm.mesero)
        .then(function (data) {
          message("Creado");
          $state.go('app.meseros')
        })
        .catch(function (response){
          message("Algo salio mal");
        })
    };

    /**
     * Funcion para mostrar un mensaje en pantalla
     * @param {any} body 
     */
    function message(body) {
      $mdToast.show({
        template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + body + '</div></md-toast>',
        hideDelay: 3000,
        position: 'top right',
        parent: '#content'
      });
    }
   
    /**
     * Funcion para cargar las sedes dependiendo del establecimiento seleccionado
    */
    function cargarSedes() {
      vm.sedes = vm.establecimientos.find(function (data) {
        if (data.id === vm.establecimiento) {
          return data
        }
      }).headquarters
      
    };
  }
})();
