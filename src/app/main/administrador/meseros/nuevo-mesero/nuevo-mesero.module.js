(function () {
  'use strict';

  angular
    .module('app.administrador.meseros.nuevo-mesero', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('app.nuevo-mesero', {
        url: '/meseros/nuevo-mesero',
        views: {
          'content@app': {
            templateUrl: '/app/main/administrador/meseros/nuevo-mesero/nuevo-mesero.html',
            controller: 'NuevoMeseroController as vm'
          }
        },
        bodyClass: 'administrador',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isAdmin: _isAdmin
        }
      });
  }

})();
