(function () {
  'use strict';

  angular
    .module('app.administrador.clientes', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('app.clientes', {
        url: '/clientes',
        views: {
          'content@app': {
            templateUrl: 'app/main/administrador/clientes/clientes.html',
            controller: 'ClientesController as vm'
          }
        },
        bodyClass: 'administrador',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isAdmin: _isAdmin
        }
      });
  }
})();
