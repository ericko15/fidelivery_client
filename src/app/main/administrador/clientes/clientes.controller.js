(function () {
  'use strict'

  angular
    .module('app.administrador.clientes')
    .controller('ClientesController', ClientesController);

  /** @ngInject */
  function ClientesController($auth, Restangular, DTOptionsBuilder, $q, DTColumnBuilder, $compile, $scope, $mdDialog, $mdToast) {
    var vm = this,
      user = $auth.getPayload().user,
      lengauge = {
        sEmptyTable: 'No hay registro de clientes',
        sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
        sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
        sInfoFiltered: '(filtrado desde _MAX_ registrsos)',
        sInfoPostFix: '',
        sInfoThousands: ',',
        sLengthMenu: 'Mostrar _MENU_ registros',
        sLoadingRecords: 'Cargando...',
        sProcessing: 'Procesando...',
        sSearch: 'Buscar:',
        sZeroRecords: 'No se encontraron registros que coincidar con la busqueda',
        oPaginate: {
          sFirst: 'Primero',
          sLast: 'Último',
          sNext: 'Siguiente',
          sPrevious: 'Anterior'
        },
        oAria: {
          sSortAscending: ': activar para ordenar las columnas ascendente',
          sSortDescending: ': activar para ordenar las columnas descendente'
        }
      };
    // Variables Globales
    vm.establecimientos = user.administrator.establishments;
    vm.envio_oferta = 'sms';
    vm.establecimiento = null;
    vm.dtInstance = {};
    vm.total_clientes = 0;
    vm.mensajes_restantes = 0;
    // Funciones globales
    vm.selecionable = seleccionable;
    vm.cargarClientes = cargarClientes;
    vm.toggleAll = toggleAll;
    vm.toggleOne = toggleOne;
    vm.enviarOferta = enviarOferta;
    vm.initTable = initTable;

    initTable();

    // Funciones Locales

    /**
     * @description Funcion para inicializar la tabla de users
     * @returns {void}
     */
    function initTable() {
      vm.seleccionables = 0;
      vm.selectedTotal = 0;
      vm.selectAll = false;
      vm.selected = {};
      vm.dtOptions = {};
      vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
        return cargarClientesPromesa();
      })
        .withPaginationType('simple')
        .withOption('responsive', {
          details: {
            type: 'column',
            target: 1
          },
          columnDefs: [ {
              className: 'control',
              orderable: false,
              targets:   0
          } ]
        })
        .withOption('createdRow', createdRow)
        .withOption('headerCallback', headerCallback)
        .withLanguage(lengauge);

      vm.dtColumns = [
        DTColumnBuilder.newColumn(null).withTitle('').notSortable().renderWith(renderSelect),
        DTColumnBuilder.newColumn('user.name').withTitle('Nombres'),
        DTColumnBuilder.newColumn('user.phone').withTitle('Contacto'),
        DTColumnBuilder.newColumn('user.gender').withTitle('Género').renderWith(renderGenero),
        DTColumnBuilder.newColumn('user.email').withTitle('Email'),
        DTColumnBuilder.newColumn('user.birthdate').withTitle('Fecha de nacimiento'),
      ]
    }

    /**
     * @description Funcion para agregar el $scope dentro de la tabla de users
     * @param {node} row elemento de fila que acaba de crearse
     * @param {Array} data Fuente de datos sin formato (matriz u objeto) para esta fila
     * @param {Integer} dataIndex El índice de la fila en el almacenamiento interno de DataTables.
     */
    function createdRow(row, data, dataIndex) {
      $compile(angular.element(row).contents())($scope);
    }

    /**
     * @description Funcion para modificar dinamicamente el encabezado de la fila
     * @param {node} header elemento del encabezado de la tabla
     */
    function headerCallback(header) {
      if (!vm.headerCompiled) {
        vm.headerCompiled = true;
        $compile(angular.element(header).contents())($scope);
      }
    }

    /**
     * @description Funcion para poner en mayusculas el genero
     * @param {*} data 
     * @param {*} type 
     * @param {*} full 
     * @param {*} meta 
     * @returns {String}
     */
    function renderGenero(data, type, full, meta) {
      return '<td>' + data.capitalize() + '</td>'
    }

    /**
     * @description Funcion para agregar un check dentro de la tabla
     * @param {*} data 
     * @param {*} type 
     * @param {*} full 
     * @param {*} meta
     * @returns {String}
     */
    function renderSelect(data, type, full, meta) {
      vm.selected[full.id] = {}
      vm.selected[full.id].selected = false
      vm.selected[full.id].selectable = data.selectable
      vm.selected[full.id].cliente = data
      if (data.selectable) {
        return '<md-checkbox aria-label="Selecionar" ng-model="vm.selected[' + data.id + '].selected" ng-click="vm.toggleOne(' + data.id + ')" ></md-checkbox>'
      } else {
        return '<td></td>'
      }
    }

    /**
     * @description Funcion que devuelve una promesa despues de consultar los datos de los users
     * @returns {Promise}
     */
    function cargarClientesPromesa() {
      var defer = $q.defer();
      if (vm.establecimiento) {
        var consulta = Restangular.one('establishments', vm.establecimiento)
      } else {
        var consulta = Restangular.one('administrators', user.administrator.id)
      }
      consulta.getList('customers').then(function (data) {
        data = data.map(function (cliente) {
          vm.mensajes_restantes = cliente.administrator.sms;
          cliente.selectable = seleccionable(cliente)
          if (cliente.selectable) {
            vm.seleccionables++
          }
          return cliente
        })
        vm.total_clientes = data.length;
        defer.resolve(data)
      })
      return defer.promise
    }

    /**
     * @description Funcion para mostrar el modal de envio de ofertas
     * @param {Object} ev
     * @returns {void}
     */
    function enviarOferta(ev) {
      var clientes = arrayClientes(vm.selected);
      var template, ctrl,
        locals = {
          clientes_totales: vm.selectedTotal,
          clientes: clientes,
          establecimiento: user.administrator.establishments.find(function (establecimiento) {
            if (establecimiento.id === vm.establecimiento) {
              return establecimiento
            }
          })
        };
      if (vm.selectedTotal > 0) {
        if (vm.establecimiento > 0) {
          if (vm.envio_oferta === 'sms') {
            template = 'app/main/administrador/clientes/dialog-oferta-sms-template.html'
            ctrl = smsCtrl
          } else {
            template = 'app/main/administrador/clientes/dialog-oferta-correo-template.html'
            ctrl = emailCtrl
          }
          dialog(ctrl, locals, template, ev)
            .then(function (response) {
              if (response) {
                vm.mensajes_restantes = response.administrator.sms
                message('Oferta enviada exitosamente - SMS Restantes=' + vm.mensajes_restantes)
                initTable()
              }
            })
        } else {
          message('<p>Debe seleccionar un establecimiento para ofertar.</p>')
        }
      } else {
        message('<p>No ha seleccionado ningun cliente.</p>')
      }
    }

    /**
     * @description Funcion para desplegar un modal
     * @param {Function} controller 
     * @param {Object} locals 
     * @param {String} template 
     * @param {Object} ev 
     * @returns {void}
     */
    function dialog(controller, locals, template, ev) {
      return $mdDialog.show({
        controller: controller,
        templateUrl: template,
        targetEvent: ev,
        locals: locals,
        clickOutsideToClose: true
      })
    }

    /**
     * @description Funcion para enviar un mensaje de texto a los user seleccionados
     * @param {Object} $scope 
     * @param {Object} $mdDialog
     * @param {Integer} clientes_totales 
     * @param {Array} clientes 
     * @param {String} establecimiento 
     * @param {Restangular} Restangular 
     * @returns {void}
     */
    function smsCtrl($scope, $mdDialog, clientes_totales, clientes, establecimiento, Restangular) {
      $scope.clientes_totales = clientes_totales
      console.log(clientes)
      $scope.form = {
        customers: clientes
      };
      $scope.closeDialog = function () {
        $mdDialog.hide()
      };
      $scope.enviar = function () {
        console.log($scope.form)
        Restangular.one('administrators', user.administrator.id)
          .post('offers', $scope.form)
          .then(function (response) {
            $mdDialog.hide(response)
          })
      }
    }

    /**
     * @description Funcion para enviar un mensaje via email a los user seleccionados
     * @param {$scope} $scope 
     * @param {$mdDialog} $mdDialog
     * @param {Integer} clientes_totales 
     * @param {Array} clientes 
     * @param {Integer} establecimiento 
     * @param {Restangular} Restangular 
     * @returns {void}
     */
    function emailCtrl($scope, $mdDialog, clientes_totales, clientes, establecimiento, Restangular) {
      $scope.clientes_totales = clientes_totales
      $scope.form = {
        clientes: clientes,
        establecimiento: establecimiento
      };
      $scope.closeDialog = function () {
        $mdDialog.hide()
      };
      $scope.enviar = function () {
        Restangular.one('administradores', user.administrator.establishments)
          .post('ofertasCorreo', $scope.form)
          .then(function (response) {
            console.log(response)
          })
      }
    }

    /**
     * @description Funcion para recargar los clientes
     * @returns {void}
     */
    function cargarClientes() {
      initTable();
      vm.dtInstance.reloadData()
    }

    /**
     * @description Funcion para saber si un user es seleccionable
     * @param {Object} cliente 
     * @returns {void}
     */
    function seleccionable(cliente) {
      return (vm.envio_oferta === 'sms' && cliente.user.phone && cliente.user.phone.length === 10) ||
        (vm.envio_oferta === 'email' && cliente.user.email && cliente.user.email.length > 10)
    }

    /**
     * @description Funcion para seleccionar todos los user que sean seleccionables
     * @param {Boolean} selectAll 
     * @param {Array} selectedItems 
     * @returns {void}
     */
    function toggleAll(selectAll, selectedItems) {
      vm.selectedTotal = 0
      for (var id in selectedItems) {
        if (selectedItems.hasOwnProperty(id)) {
          if (selectedItems[id].selectable) {
            selectedItems[id].selected = !selectAll
            if (!selectAll) {
              vm.selectedTotal++
            } else {
              vm.selectedTotal = 0
            }
          }
        }
      }
    }

    /**
     * @description Funcion para seleccionar un user
     * @param {String} id 
     * @returns {void}
     */
    function toggleOne(id) {
      if (!vm.selected[id].selected) {
        vm.selectedTotal++
      } else {
        vm.selectedTotal--
      }
      vm.selectAll = vm.seleccionables === vm.selectedTotal
    }

    /**
     * @description Funcion para mostrar un mensaje en pantalla
     * @param {String} body 
     * @returns {void}
     */
    function message(body) {
      $mdToast.show({
        template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + body + '</div></md-toast>',
        hideDelay: 3000,
        position: 'bottom left',
        parent: '#content'
      })
    }

    /**
     * @description Funcion para obtener los users seleccionados
     * @param {Array} seleccionados 
     * @returns {void}
     */
    function arrayClientes(seleccionados) {
      var array = []
      for (var cliente in seleccionados) {
        if (vm.selected[cliente].selected) {
          array.push({id: vm.selected[cliente].cliente.id})
        }
      }
      return array
    }

  }
})();
