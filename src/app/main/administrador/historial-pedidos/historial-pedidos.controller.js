(function () {
  'use strict';
  angular.module('app.administrador.historial-pedidos').controller('HistorialPedidosController', HistorialPedidosController)

  /** @ngInject */
  function HistorialPedidosController($filter, Restangular, $auth, CalculoValoresService, ImprimirPedidoService) {
    var vm = this,
      user = $auth.getPayload().user,
      pedidos = Restangular.all('administrators').all('orders');

    // Variables Globales
    vm.fecha = new Date();
    vm.total = 0;
    vm.establecimientos = user.administrator.establishments;
    vm.establecimiento = user.administrator.establishments[0].id
    vm.sedes = user.administrator.establishments[0].headquarters
    vm.dtInstance = {};
    vm.dtOptions = {
      columnDefs: [{
        responsivePriority: 1,
        filterable: false,
        sortable: false
      }],
      autoWidth: false,
      language: {
        sEmptyTable: "No hay registro despachado en la fecha seleccionada",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
        sInfoFiltered: "(filtrado desde _MAX_ registrsos)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLengthMenu: "Mostrar _MENU_ registros",
        sLoadingRecords: "Cargando...",
        sProcessing: "Procesando...",
        sSearch: "Buscar:",
        sZeroRecords: "No se encontraron registros que coincidar con la busqueda",
        oPaginate: {
          sFirst: "Primero",
          sLast: "Último",
          sNext: "Siguiente",
          sPrevious: "Anterior"
        },
        oAria: {
          sSortAscending: ": activar para ordenar las columnas ascendente",
          sSortDescending: ": activar para ordenar las columnas descendente"
        }
      },
      pagingType: 'simple',
      lengthMenu: [10, 20, 30, 50, 100],
      pageLength: 10,
      responsive: {
        details: {
          type: 'column',
          target: 0
        }
      }
    }

    // Funciones Globales
    vm.cargarPedidosEnviadosDia = cargarPedidosEnviadosDia;
    vm.getDatePedido = getDatePedido;
    vm.seleccionarEstablecimiento = seleccionarEstablecimiento;
    vm.getValorPedidos = getValorPedidos;

    /**
     * @description Funcion de inicializacion del controlador
     * @returns {void}
     */
    function init() {
      cargarPedidosEnviadosDia()
    }

    init();

    /**
     * @description Funcion para cargar las de un establecimiento 
     * @returns {void}
     */
    function seleccionarEstablecimiento() {
      if (vm.establecimiento != null) {
        vm.sedes = vm.establecimientos.find(function (data) {
          return data.id === vm.establecimiento
        }).headquarters
        vm.sede = null
      }
      cargarPedidosEnviadosDia()
    }

    /**
     * @description Funcion para cargar los pedidos de un dia especifico
     * @returns {void}
     */
    function cargarPedidosEnviadosDia() {
      var initialDate = (vm.fecha) ? initialDate = formato_fecha(vm.fecha) : initialDate = formato_fecha(new Date())
      var establecimientoId = (!vm.establecimiento) ? null : vm.establecimiento
      var sedeId = (!vm.sede) ? null : vm.sede
      vm.pedidos = []
      vm.total = 0
      pedidos.getList({
        establishment_id: establecimientoId,
        headquarter_id: sedeId,
        initialDate: initialDate
      }).then(function (data) {
        if (data.length) {
          vm.pedidos = data.map(function (pedido) {
            if (pedido.type === 2) {
              pedido.type = 'Domicilio'
            } else {
              pedido.type = 'Mesa'
            }
            if (!pedido.detail) {
              pedido.detail = 'No registrado'
            }
            return pedido
          })
          vm.total = getValorPedidos()
        }
      })
    }

    /**
     * @description Funcion para convertir la fecha de actualizacion del pedido a un Date
     * @returns {void}
     */
    function getDatePedido(pedido, atributo) {
      return new Date(pedido.updated_at)
    }

    /**
     * @description Funcion para convertir el valor del pedido a un valo de moneda
     * @returns {void}
     */
    function getValorPedidos() {
      return $filter('currency')(CalculoValoresService.getValorPedidos(vm.pedidos), '', 0)
    }
  }

})();
