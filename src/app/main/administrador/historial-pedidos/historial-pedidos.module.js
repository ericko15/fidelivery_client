(function () {
  'use strict';
  angular.module('app.administrador.historial-pedidos', []).config(config);
  /** @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('app.historial-pedidos', {
        url: '/historial-pedidos',
        views: {
          'content@app': {
            templateUrl: 'app/main/administrador/historial-pedidos/historial-pedidos.html',
            controller: 'HistorialPedidosController as vm'
          }
        },
        bodyClass: 'administrador',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isAdmin: _isAdmin
        }
      })
  }

})();
