(function () {
  'use strict';

  angular
    .module('app.administrador.informacion-cuenta', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('app.informacion-cuenta', {
        url: '/informacion-cuenta',
        views: {
          'content@app': {
            templateUrl: 'app/main/administrador/informacion-cuenta/informacion-cuenta.html',
            controller: 'InformacionCuentaController as vm'
          }
        },
        bodyClass: 'administrador',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isAdmin: _isAdmin
        }
      });
  }

})();
