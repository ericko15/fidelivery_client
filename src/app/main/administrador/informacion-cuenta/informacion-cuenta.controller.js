(function () {
  'use strict';

  angular.module('app.administrador.informacion-cuenta')
    .controller('InformacionCuentaController', InformacionCuentaController);

  /** @ngInject */
  function InformacionCuentaController(Restangular, $mdToast, $mdDialog, $auth) {
    var vm = this;
    // Variables Globales
    vm.user = $auth.getPayload().user;
    // Funciones Globales
    vm.confirmar = confirmar;

    /**
     * @description Funcion para mostrar un modal de confirmacion
     * @param {Object} ev 
     * @returns {void}
     */
    function confirmar(ev) {
      var generoPalabraSeguro = vm.user.genero == 'masculino' ?
        'seguro' : (vm.user.genero == 'femenino' ? 'segura' : 'seguro(a)');
      vm.activated = true;
      var confirm = $mdDialog.confirm()
        .title('Confirmar actualización de datos')
        .htmlContent('¿Está ' + generoPalabraSeguro + ' que desea actualizar sus datos?')
        .targetEvent(ev)
        .ok('SÍ')
        .cancel('CANCELAR');

      $mdDialog.show(confirm)
        .then(actualizarDatos);
    }

    /**
     * @description Funcion para actualizar los datos del usuario
     * @returns {void}
     */
    function actualizarDatos() {
      Restangular.one('users', vm.user.administrator.id)
        .patch(vm.user).then(function (data) {
          if (data.result) {
            message('Actualizado correctamente');
          } else {
            var mensaje = (data.validator) ?
              data.validator.join('<br />') : '';
            message(mensaje);
          }
        }).catch(function (response) {
          message(response.data.message);
        });
    }

    /**
     * @description Funcion para mostrar un mensaje en pantalla
     * @param {String} body
     * @returns {void}
     */
    function message(body) {
      $mdToast.show({
        template: '<md-toast id="' + body + '" layout="column" layout-align="center start">' +
          '<div class="md-toast-content">' + body + '</div></md-toast>',
        hideDelay: 5000,
        position: 'top right',
        parent: '#content'
      });
    }
  }

})();
