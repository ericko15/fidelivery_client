(function () {
  'use strict';

  angular
    .module('app.administrador.reporte-graficas')
    .controller('ReporteGraficasController', ReporteGraficasController);

  /** @ngInject */
  function ReporteGraficasController(Restangular, $auth, $mdToast, $filter) {
    var vm = this;
    var fecha = new Date();

    vm.user = $auth.getPayload().user;
    vm.seleccionarEstablecimiento = seleccionarEstablecimiento;
    vm.cargarTodo = cargarTodo;
    vm.establecimientos = vm.user.administrator.establishments;
    vm.establecimiento = vm.user.administrator.establishments[0].id;
    vm.sedes = vm.user.administrator.establishments[0].headquarters;

    function init() {
      cargarTodo();
    }

    /**
     * @description Funcion para cargar todas las graficas
     */
    function cargarTodo() {
      cargarPedidosPorDia();
      cargarClientesPorGenero();
      cargarPedidosDiaSemana();
      cargarTopProductosVendidos();
    }

    /**
     * @description Funcion para cargar las sedes del establecimiento seleccionado
    */
    function seleccionarEstablecimiento() {
      if (vm.establecimiento != null) {
        vm.sedes = vm.establecimientos.find(function (data) {
          return data.id === vm.establecimiento
        }).headquarters
        vm.sede = null
      }
      vm.cargarTodo()
    }

    /**
     * @description Funcion para cargar los pedidos realizados en un lapso de tiempo mostrandolos dia a dia
     */
    function cargarPedidosPorDia() {
      var initialDate = (vm.initialDate) ? formato_fecha(vm.initialDate) : formato_fecha(new Date())
      var finalDate = (vm.finalDate) ? formato_fecha(vm.finalDate) : formato_fecha(new Date())
      var establecimientoId = (!vm.establecimiento) ? null : vm.establecimiento
      var sedeId = (!vm.sede) ? null : vm.sede
      Restangular.all('administrators').all('orders')
        .getList({
          days: true,
          establishment_id: establecimientoId,
          headquarter_id: sedeId,
          initialDate: initialDate,
          finalDate: finalDate
        }).then(function (data) {
          vm.pedidos_dia = {};
          vm.pedidos_dia.datasetOverride = [{
            yAxisID: 'y-axis-1'
          }, {
            yAxisID: 'y-axis-2'
          }];
          vm.pedidos_dia.series = ['Cantidad', 'Valor'];
          vm.pedidos_dia.options = {
            legend: {
              display: true
            },
            scales: {
              yAxes: [{
                display: true,
                id: 'y-axis-1',
                position: 'left',
                ticks: {
                  beginAtZero: true
                },
                type: 'linear'
              },
              {
                display: true,
                id: 'y-axis-2',
                position: 'right',
                ticks: {
                  beginAtZero: true
                },
                type: 'linear'
              }
              ]
            }
          };
          vm.pedidos_dia.labels = getPropertyInArrayObject(data, 'fecha').map(function (fecha) {
            return $filter('date')(new Date(fecha), 'dd-MMM', '+0000')
          });
          vm.pedidos_dia.data = [
            getPropertyInArrayObject(data, 'dispatched'),
            parseIntArray(getPropertyInArrayObject(data, 'total'))
          ];
          vm.pedidos_dia.colors = ['#45b7cd', '#ff6384'];
          vm.pedidos_dia.cantidad = sumarElementosArray(vm.pedidos_dia.data[0]);
          vm.pedidos_dia.valor = sumarElementosArray(vm.pedidos_dia.data[1]);
          vm.pedidos = [];
          vm.pedidos = data
        })
        .catch(function (error) {
          var mensaje = (!error.status) ? error : String.format('Error: {0} {1}', error.status, error.statusText);
          message(mensaje);
        })
    }

    /** 
     * @description Funcion para cargar los pedidos por dia de la semana en un lapso de tiempo
    */
    function cargarPedidosDiaSemana() {
      var initialDate = (vm.initialDate) ? formato_fecha(vm.initialDate) : formato_fecha(new Date())
      var finalDate = (vm.finalDate) ? formato_fecha(vm.finalDate) : formato_fecha(new Date())
      var establecimientoId = (!vm.establecimiento) ? null : vm.establecimiento
      var sedeId = (!vm.sede) ? null : vm.sede
      Restangular.all('administrators').one('orders')
        .get({
          weekdays: true,
          establishment_id: establecimientoId,
          headquarter_id: sedeId,
          initialDate: initialDate,
          finalDate: finalDate
        }).then(function (data) {
          vm.pedidos_dias_semanas = {
            total: 0
          };
          vm.pedidos_dias_semanas.labels = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
          vm.pedidos_dias_semanas.options = {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true,
                  stepSize: 2
                }
              }]
            }
          };
          vm.pedidos_dias_semanas.data = [
            data.Lunes,
            data.Martes,
            data.Miercoles,
            data.Jueves,
            data.Viernes,
            data.Sabado,
            data.Domingo
          ];
          vm.pedidos_dias_semanas.data.map(function (value) {
            vm.pedidos_dias_semanas.total += value
          });
        })
        .catch(function (error) {
          var mensaje = (!error.status) ? error : String.format('Error: {0} {1}', error.status, error.statusText);
          message(mensaje)
        })
    }

    /**
     * @description Funcion para cargar la cantidad de clientes por genero
     */
    function cargarClientesPorGenero() {
      Restangular.one('establishments', vm.establecimiento)
        .all('customers').getList({
          forGender: true
        }).then(function (data) {
          vm.clientes_generos = []
          vm.clientes_generos.labels = [data[0].gender.capitalize(), data[1].gender.capitalize()];
          vm.clientes_generos.data = [data[0].quantity, data[1].quantity];
          vm.clientes_generos.options = {
            legend: {
              display: true,
              position: 'right'
            }
          };
          vm.clientes_generos.total = data[0].quantity + data[1].quantity
        })
        .catch(function (error) {
          var mensaje = (!error.status) ? error : String.format('Error: {0} {1}', error.status, error.statusText);
          message(mensaje)
        })
    }

    /**
     * @description Funcion para cargar el top de productos vendidos
     */
    function cargarTopProductosVendidos() {
      var initialDate = (vm.initialDate) ? formato_fecha(vm.initialDate) : formato_fecha(new Date())
      var finalDate = (vm.finalDate) ? formato_fecha(vm.finalDate) : formato_fecha(new Date())
      var establecimientoId = (!vm.establecimiento) ? null : vm.establecimiento;
      var sedeId = (!vm.sede) ? null : vm.sede;
      if (!sedeId) {
        var consulta = Restangular.one('establishments', establecimientoId)
          .all('products')
      } else {
        var consulta = Restangular.one('establishments', establecimientoId)
          .one('headquarters', sedeId)
          .all('products')
      }
      consulta.getList({
        soldProducts: true,
        initialDate: initialDate,
        finalDate: finalDate
      }).then(function (data) {
        data = data.slice(0, 10);
        vm.productos_vendidos = {};
        vm.productos_vendidos.labels = getPropertyInArrayObject(data, 'name');
        vm.productos_vendidos.data = getPropertyInArrayObject(data, 'quantity_sold');
        vm.productos_vendidos.options = {
          legend: {
            display: false
          }
        };
        vm.productos_vendidos.total = 0
      })
        .catch(function (error) {
          var mensaje = (!error.status) ? error : String.format('Error: {0} {1}', error.status, error.statusText);
          message(mensaje)
        })
    }

    function message(body) {
      $mdToast.show({
        template: '<md-toast id="' + body + '" layout="column" layout-align="center start">' +
          '<div class="md-toast-content">' + body + '</div></md-toast>',
        hideDelay: 5000,
        position: 'top right',
        parent: '#content'
      })
    }

    init()
  }
})();
