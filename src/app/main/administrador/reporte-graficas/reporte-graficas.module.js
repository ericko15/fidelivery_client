(function () {
  'use strict';

  angular
    .module('app.administrador.reporte-graficas', ['chart.js'])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('app.reporte-graficas', {
        url: '/reporte-graficas',
        views: {
          'content@app': {
            templateUrl: 'app/main/administrador/reporte-graficas/reporte-graficas.html',
            controller: 'ReporteGraficasController as vm'
          }
        },
        bodyClass: 'administrador',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isAdmin: _isAdmin
        }
      });
  }

})();
