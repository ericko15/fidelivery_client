(function () {
  'use strict';

  angular
    .module('app.vendedor.pedidos')
    .controller('CumpleanosController', CumpleanosController);

  /** @ngInject */
  function CumpleanosController($scope, $mdDialog) {
    var vm = this;

    vm.dtInstance = {};
    vm.dtOptions = {
      dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
      columnDefs: [
        {
          // Target the id column
          targets: 0,
          width: '72px'
        },
        {
          // Target the image column
          targets: 1,
          filterable: false,
          sortable: false,
          width: '80px'
        },
        {
          // Target the actions column
          targets: 7,
          responsivePriority: 1,
          filterable: false,
          sortable: false
        }
      ],
      initComplete: function () {
        var api = this.api(),
          searchBox = angular.element('body').find('#vendedor-cumpleanos-search');

        // Bind an external input as a table wide search box
        if (searchBox.length > 0) {
          searchBox.on('keyup', function (event) {
            api.search(event.target.value).draw();
          });
        }
      },
      autoWidth: false,
      language: {
        sEmptyTable: "No hay pedidos en cola",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
        sInfoFiltered: "(filtrado desde _MAX_ registrsos)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLengthMenu: "Mostrar _MENU_ registros",
        sLoadingRecords: "Cargando...",
        sProcessing: "Procesando...",
        sSearch: "Buscar:",
        sZeroRecords: "No se encontraron registros que coincidar con la busqueda",
        oPaginate: {
          sFirst: "Primero",
          sLast: "Último",
          sNext: "Siguiente",
          sPrevious: "Anterior"
        },
        oAria: {
          sSortAscending: ": activar para ordenar las columnas ascendente",
          sSortDescending: ": activar para ordenar las columnas descendente"
        }
      },
      pagingType: 'simple',
      lengthMenu: [10, 20, 30, 50, 100],
      pageLength: 20,
      scrollY: 'auto',
      responsive: true
    };

    vm.showDialogFelicitar = function (ev, data) {

      $scope.mensaje = "";
      $mdDialog.show({
        controller: function ($scope, $mdDialog, datos) {
          $scope.mensaje = datos.nombre_completo + " Restaurante Local quiere felicitarte en tu " +
            "cumpleaños y desearte muchos éxitos. Ven y disfruta de nuestra promoción especial para ti.";
          $scope.closeDialog = function () {
            $mdDialog.hide();
          };
        },
        templateUrl: 'app/main/vendedor/cumpleanos/dialog-felicitaciones-template.html',
        targetEvent: ev,
        locals: {
          datos: data
        },
        clickOutsideToClose: true
      });

    };


    vm.cumpleanos = [
    ];
  }
})();


