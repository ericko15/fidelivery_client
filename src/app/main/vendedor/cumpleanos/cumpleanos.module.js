(function () {
  'use strict';
  angular.module('app.vendedor.cumpleanos', []).config(config);
  /** @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('app.cumpleanos', {
        url: '/cumpleanos',
        views: {
          'content@app': {
            templateUrl: 'app/main/vendedor/cumpleanos/cumpleanos.html',
            controller: 'CumpleanosController as vm'
          }
        },
        bodyClass: 'vendedor',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isAdmin: _isAdmin
        }
      })
  }
})();
