(function () {
  'use strict';

  angular
    .module('app.vendedor.pedidos')
    .controller('PedidosController', PedidosController);

  /** @ngInject */
  function PedidosController($mdDialog, Restangular, $auth, ImprimirPedidoService, CalculoValoresService, $mdToast) {
    var vm = this,
      user = $auth.getPayload().user;
    vm.dtInstance = {};
    vm.dtOptions = {
      columnDefs: [{
        responsivePriority: 1,
        filterable: false,
        sortable: false,
        width: '120px'
      }],
      initComplete: function () {
        var api = this.api(),
          searchBox = angular.element('body').find('#vendedor-pedidos-search');

        // Bind an external input as a table wide search box
        if (searchBox.length > 0) {
          searchBox.on('keyup', function (event) {
            api.search(event.target.value).draw()
          })
        }
      },
      autoWidth: false,
      language: {
        sEmptyTable: 'No hay pedidos en cola',
        sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
        sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
        sInfoFiltered: '(filtrado desde _MAX_ registrsos)',
        sInfoPostFix: '',
        sInfoThousands: ',',
        sLengthMenu: 'Mostrar _MENU_ registros',
        sLoadingRecords: 'Cargando...',
        sProcessing: 'Procesando...',
        sSearch: 'Buscar:',
        sZeroRecords: 'No se encontraron registros que coincidar con la busqueda',
        oPaginate: {
          sFirst: 'Primero',
          sLast: 'Último',
          sNext: 'Siguiente',
          sPrevious: 'Anterior'
        },
        oAria: {
          sSortAscending: ': activar para ordenar las columnas ascendente',
          sSortDescending: ': activar para ordenar las columnas descendente'
        }
      },
      pagingType: 'simple',
      lengthMenu: [10, 20, 30, 50, 100],
      pageLength: 10,
      responsive: {
        details: {
          type: 'column',
          target: 1
        }
      }
    };
    vm.getValorIVA = CalculoValoresService.getValorIVA
    vm.user = user
    vm.pedido = {}
    vm.imprimir = false
    vm.showDialogNuevoPedido = showDialogNuevoPedido;
    vm.despachar = despachar
    vm.showDialogEliminar = showDialogEliminar
    vm.showDialogEditarPedido = showDialogEditarPedido
    vm = ImprimirPedidoService.init(vm)

    function init() {
      cargarPedidos()
    }

    function showDialogNuevoPedido(ev) {
      $mdDialog.show({
        controller: nuevoPedidoCtrl,
        templateUrl: 'app/main/vendedor/pedidos/dialog-nuevo-pedido-template.html',
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          user: user,
          pedido: null
        }
      }).then(function (data) {
        if (data) {
          vm.esComanda = true
          vm.imprimirPedido(ev, data)
          cargarPedidos()
        }
      })
    }

    function showDialogEditarPedido(ev, pedido) {
      $mdDialog.show({
        controller: nuevoPedidoCtrl,
        templateUrl: 'app/main/vendedor/pedidos/dialog-editar-pedido-template.html',
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          user: user,
          pedido: pedido
        }
      }).then(function (data) {
        if (data) {
          vm.esComanda = true
          vm.imprimirPedido(ev, data)
          cargarPedidos()
        }
      })
    }

    function nuevoPedidoCtrl($scope, $mdDialog, user, Restangular, pedido) {
      // Variables globales
      $scope.sede = user.employee.headquarter.id
      $scope.establecimiento = user.employee.headquarter.establishment_id
      $scope.mesas = user.employee.headquarter.tables
      $scope.meseros = JSON.parse(localStorage.getItem('meseros'))
      $scope.clientes = JSON.parse(localStorage.getItem('clientes'))
      $scope.nombre_sede = user.employee.headquarter.name
      $scope.estado_vizulizacion = false
      $scope.clase_agregar_informacion = 'icon-account-plus'
      $scope.aviso_agregar_informacion = 'Agregar información del cliente'
      $scope.productos = JSON.parse(localStorage.getItem('productos'))
      $scope.cliente = {}
      $scope.cliente.nuevo = false
      $scope.productosSeleccionados = []
      // Funciones globales
      $scope.closeDialog = closeDialog
      $scope.motrarInformacion = motrarInformacion
      $scope.seleccionarTipo = seleccionarTipo
      $scope.agregarProducto = agregarProducto
      $scope.precioTotal = precioTotal
      $scope.nuevoProducto = nuevoProducto
      $scope.eliminarProducto = eliminarProducto
      $scope.nuevoCliente = nuevoCliente
      $scope.buscarCliente = buscarCliente
      $scope.agregarCliente = agregarCliente
      $scope.crear = crear
      $scope.editar = editar
      if (pedido == null) {
        $scope.pedido = { type: 1 }
        $scope.pedido.table_id = $scope.mesas[0].id
        $scope.pedido.employee_id = $scope.meseros[0].id
      } else {
        cargarPedido()
      }

      function cargarPedido() {
        $scope.pedido = pedido
        if ($scope.pedido.detail == null) {
          $scope.pedido.detail = ''
        }
        $scope.productosSeleccionados = pedido.products.map(function (product) {
          return {
            id: product.id,
            name: product.name,
            price: product.price,
            quantity: product.pivot.quantity,
            total: product.price * product.pivot.quantity
          }
        })
        if ($scope.pedido.customer != null) {
          $scope.estado_vizulizacion = true
          $scope.customer = pedido.customer.user
          $scope.customer.id = parseInt(pedido.customer)
          $scope.customer.birthdate = new Date(pedido.customer.user.birthdate)
          $scope.cliente.nuevo = true
        }
      }

      function closeDialog() {
        $mdDialog.hide()
      }

      function motrarInformacion() {
        if ($scope.estado_vizulizacion === true) {
          $scope.estado_vizulizacion = false;
          $scope.clase_agregar_informacion = 'icon-account-plus';
          $scope.aviso_agregar_informacion = 'Agregar información del cliente';
        } else {
          $scope.estado_vizulizacion = true;
          $scope.clase_agregar_informacion = 'icon-account-minus';
          $scope.aviso_agregar_informacion = 'Quitar información del cliente';
        }
      }

      function seleccionarTipo() {
        if ($scope.pedido.type == 1) {
          $scope.pedido.table_id = $scope.mesas[0].id
          $scope.pedido.employee_id = $scope.meseros[0].id
        } else {
          delete $scope.pedido.table_id
          delete $scope.pedido.employee_id
        }
      }

      function agregarProducto($item) {
        if ($item) {
          var producto = angular.copy($item.originalObject);
          producto.quantity = 1
          producto.total = producto.price * producto.quantity
          var encontrado = $scope.productosSeleccionados.find(function (data) {
            if (data.id == producto.id) {
              data.quantity += 1
              data.total = data.price * data.quantity
              return data
            }
          })
          if (!encontrado) {
            $scope.productosSeleccionados.unshift(producto);
          }
          calcularTotal()
        }
      }

      function precioTotal(index) {
        $scope.productosSeleccionados[index].total = $scope.productosSeleccionados[index].price * $scope.productosSeleccionados[index].quantity
        calcularTotal()
      }

      function calcularTotal() {
        $scope.pedido.total = 0
        $scope.productosSeleccionados.forEach(function (producto) {
          $scope.pedido.total += producto.total
        })
      }

      function eliminarProducto(index) {
        console.log(index)
        $scope.productosSeleccionados.splice(index, 1)
        calcularTotal()
      }

      function nuevoProducto() {
        var name = document.getElementById("busqueda-producto_value");
        $scope.productosSeleccionados.unshift({
          name: name.value,
          price: 0,
          quantity: 1,
          total: 0
        });
        $scope.$broadcast('angucomplete-alt:clearInput', 'busqueda-producto');
      }

      function filtrarProductoSeleccionados() {
        return $scope.productosSeleccionados.map(function (producto) {
          if (producto.id) {
            return {
              id: producto.id,
              price: producto.price,
              quantity: producto.quantity
            }
          } else {
            return {
              name: producto.name,
              price: producto.price,
              quantity: producto.quantity,
              category_id: 1
            }
          }
        })
      }

      function agregarCliente($item) {
        if ($item) {
          $scope.customer = angular.copy($item.originalObject.user);
          $scope.customer.id = angular.copy($item.originalObject.id)
          if ($scope.customer.birthdate) {
            $scope.customer.birthdate = new Date()
          }
          $scope.cliente.nuevo = true
        }
      }

      function nuevoCliente() {
        $scope.customer = {}
        $scope.cliente.nuevo = true
      }

      function buscarCliente() {
        $scope.customer = {}
        $scope.cliente.nuevo = false
      }

      function clienteNuevoExistente() {
        if ($scope.customer) {
          if ($scope.customer.id) {
            $scope.pedido.customer_id = $scope.customer.id
            delete $scope.customer.id
            $scope.pedido.customer = $scope.customer
          } else {
            $scope.pedido.customer = $scope.customer
          }
        }
      }

      function crear() {
        if ($scope.pedido.type == 2) {
          $scope.pedido.employee_id = user.employee.id
        }
        $scope.pedido.products = filtrarProductoSeleccionados()
        clienteNuevoExistente()
        $scope.pedido.subtotal = $scope.pedido.total
        Restangular.one('establishments', user.employee.headquarter.establishment_id)
          .one('headquarters', user.employee.headquarter_id)
          .all('orders')
          .post($scope.pedido)
          .then(function (response) {
            $mdDialog.hide(response)
          })
          .catch(function (err) {
            console.log(err)
          })
      }

      function editar() {
        if ($scope.pedido.type == 2) {
          $scope.pedido.employee_id = user.employee.id
        }
        $scope.pedido.products = filtrarProductoSeleccionados()
        clienteNuevoExistente()
        $scope.pedido.subtotal = $scope.pedido.total
        Restangular.one('establishments', user.employee.headquarter.establishment_id)
          .one('headquarters', user.employee.headquarter_id)
          .one('orders', $scope.pedido.id)
          .patch($scope.pedido)
          .then(function (response) {
            $mdDialog.hide(response)
          })
          .catch(function (err) {
            console.log(err)
          })
      }
    }

    function cargarPedidos() {
      Restangular.all('administrators')
        .all('orders')
        .getList({
          status: 1
        })
        .then(function (data) {
          vm.pedidos = data
        })
    }

    function despachar(ev, pedido) {
      var pedido_copia = Restangular.copy(pedido)
      Restangular.one('establishments', user.employee.headquarter.establishment_id)
          .one('headquarters', user.employee.headquarter_id)
          .one('orders', pedido_copia.id)
          .patch({
            status: 2
          })
          .then(function (response) {
            if (pedido.type == 2) {
              message('Pedido #' + pedido.consecutive + " Despachado y mensaje enviado")
            } else {
              message('Pedido #' + pedido.consecutive + " Despachado")
            }
            cargarPedidos()
          })
          .catch(function (err) {
            message(err.message)
          })
    }

    /**
     * Funcion para mostrar la pantalla de eliminar pedido
     * @param {any} ev 
     * @param {any} data 
     */
    function showDialogEliminar(ev, pedido) {
      $mdDialog.show({
        controller: EliminarController,
        templateUrl: 'app/main/vendedor/pedidos/dialog-eliminar-pedido-template.html',
        targetEvent: ev,
        locals: {
          datos: pedido,
          user: $auth.getPayload().user
        },
        clickOutsideToClose: true
      }).then(function (data) {
        if (data) {
          cargarPedidos()
        }
      })
    };

    /**
     * Funcion controladora de la pantalla de eliminar pedido
     */
    function EliminarController($scope, $mdDialog, datos, user) {
      $scope.number = datos.consecutive
      $scope.closeDialog = function () {
        $mdDialog.hide()
      };
      $scope.remove = function () {
        Restangular.one('establishments', user.employee.headquarter.establishment_id)
          .one('headquarters', user.employee.headquarter_id)
          .one('orders', datos.id)
          .remove()
          .then(function (response) {
            $mdDialog.hide(response)
          })
          .catch(function (err) {
            console.log(err)
          })
      }
    }

    /**
     * @description Funcion para mostrar un mensaje en pantalla
     * @param {String} body 
     * @returns {void}
     */
    function message(body) {
      $mdToast.show({
        template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + body + '</div></md-toast>',
        hideDelay: 3000,
        position: 'bottom left',
        parent: '#content'
      })
    }

    init();
  }
})();
