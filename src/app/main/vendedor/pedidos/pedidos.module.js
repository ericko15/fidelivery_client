(function () {
  'use strict';
  angular.module('app.vendedor.pedidos', []).config(config);
  /** @ngInject */
  function config($stateProvider) {
    // State
    $stateProvider
      .state('app.pedidos', {
        url: '/pedidos',
        views: {
          'content@app': {
            templateUrl: 'app/main/vendedor/pedidos/pedidos.html',
            controller: 'PedidosController as vm'
          }
        },
        bodyClass: 'vendedor',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isVendedor: _isVendedor
        }
      });
  }
})();
