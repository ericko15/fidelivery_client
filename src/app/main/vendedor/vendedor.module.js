(function () {
    'use strict';

    angular
        .module('app.vendedor', [
            'app.vendedor.pedidos',
            'app.vendedor.resumen-dia',
            'app.vendedor.cumpleanos'
        ])
        .config(config);

    function config(msNavigationServiceProvider) {

        msNavigationServiceProvider.saveItem('pedidos', {
            title: 'Realizar pedido',
            icon: 'icon-cart',
            state: 'app.pedidos',
            bodyClass: 'vendedor',
            rol: 'VENDEDOR'
        });

        msNavigationServiceProvider.saveItem('resumen-dia', {
            title: 'Resumen del día',
            icon: 'icon-book-open',
            state: 'app.resumen-dia',
            bodyClass: 'vendedor',
            rol: 'VENDEDOR'
        });

        /*msNavigationServiceProvider.saveItem('cumpleanos', {
            title: 'Cumpleaños',
            icon: 'icon-cake-variant',
            state: 'app.cumpleanos',
            bodyClass: 'vendedor',
            rol: 'ADMIN'
        });*/
    }
})();
