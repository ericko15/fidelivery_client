(function () {
  'use strict';
  angular.module('app.vendedor.resumen-dia', []).config(config);
  /** @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('app.resumen-dia', {
        url: '/resumen-dia',
        views: {
          'content@app': {
            templateUrl: 'app/main/vendedor/resumen-dia/resumen-dia.html',
            controller: 'ResumenDiaController as vm'
          }
        },
        bodyClass: 'vendedor-resumen-dia',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
          isAdmin: _isAdmin
        }
      })
  }
})();
