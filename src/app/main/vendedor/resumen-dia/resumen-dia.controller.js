(function () {
  'use strict';

  angular
    .module('app.vendedor.resumen-dia')
    .controller('ResumenDiaController', ResumenDiaController);

  /** @ngInject */
  function ResumenDiaController(Restangular, $auth, $mdDialog, ImprimirPedidoService) {
    var vm = this,
      pedidos = Restangular.all('administrators').all('orders'),
      user = JSON.parse(localStorage.getItem('user'));
    vm.dtInstance = {};
    vm.dtOptions = {
      columnDefs: [
        {
          responsivePriority: 1,
          filterable: false,
          sortable: false
        }
      ],
      initComplete: function () {
        var api = this.api(),
          searchBox = angular.element('body').find('#vendedor-resumen-dia-search');

        // Bind an external input as a table wide search box
        if (searchBox.length > 0) {
          searchBox.on('keyup', function (event) {
            api.search(event.target.value).draw();
          });
        }
      },
      autoWidth: false,
      language: {
        sEmptyTable: "No hay registro despachado en la fecha seleccionada",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
        sInfoFiltered: "(filtrado desde _MAX_ registrsos)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLengthMenu: "Mostrar _MENU_ registros",
        sLoadingRecords: "Cargando...",
        sProcessing: "Procesando...",
        sSearch: "Buscar:",
        sZeroRecords: "No se encontraron registros que coincidar con la busqueda",
        oPaginate: {
          sFirst: "Primero",
          sLast: "Último",
          sNext: "Siguiente",
          sPrevious: "Anterior"
        },
        oAria: {
          sSortAscending: ": activar para ordenar las columnas ascendente",
          sSortDescending: ": activar para ordenar las columnas descendente"
        }
      },
      pagingType: 'simple',
      lengthMenu: [10, 20, 30, 50, 100],
      pageLength: 10,
      deferRender: true,
      responsive: {
        details: {
          type: 'column',
          target: 1
        }
      }
    };
    vm.establecimientos = buscarEstablecimientos();
    vm.seleccionarEstablecimiento = seleccionarEstablecimiento
    vm.cargarPedidosEnviadosDia = cargarPedidosEnviadosDia

    function cargarPedidosEnviadosDia() {
      var initialDate = (vm.initialDate) ? initialDate = formato_fecha(vm.initialDate) : initialDate = formato_fecha(new Date())
      vm.total_ingresos = 0
      vm.cantidad_domicilio = 0
      vm.cantidad_presencial = 0
      vm.pedidos = [];
      pedidos.getList({
        status: 2,
        establishment_id: user.employee.headquarter.establishment.id,
        headquarter_id: user.employee.headquarter.id,
        initialDate: initialDate
      }).then(function (response) {
        if (response.length > 0) {
          vm.pedidos = response.map(function (data) {
            if (data.type === 2) {
              vm.cantidad_domicilio += 1
              data.type = 'Domicilio'
            } else if (data.type === 1) {
              data.type = 'Mesa'
              vm.cantidad_presencial += 1
            }
            vm.total_ingresos += data.total
            return data
          });
          vm.cantidad_despachada = response.length
        }
      }).catch(function (error) {
        var mensaje = String.format('Error: {0} {1}', error.status, error.statusText);
      })
    };

    function seleccionarEstablecimiento() {
      if (user.rol === 3) {
        if (vm.establecimiento != null) {
          vm.sedes = [user.vendedor.sede]
        }
      } else if (user.rol === 2) {
        if (vm.establecimiento != null) {
          vm.sedes = vm.establecimientos.find(function (data) {
            return data.id === vm.establecimiento
          }).headquarters
          vm.sede = null
        }
      }
      if (vm.initialDate) {
        cargarPedidosEnviadosDia()
      }
    };

    function buscarEstablecimientos() {
      if (user.rol === 3) {
        return [user.employee.headquarter.establishment]
      } else if (user.rol === 2) {
        return user.administrator.establishments
      }
    };

    vm = ImprimirPedidoService.init(vm)

    cargarPedidosEnviadosDia()

  }
})();
