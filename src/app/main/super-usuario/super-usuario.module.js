(function () {
    'use strict';

    angular.module('app.super-usuario', [
        'app.super-usuario.usuarios',
        'app.super-usuario.establecimientos',
        'app.super-usuario.sedes'
    ]).config(config);

    /** @ngInject */
    function config(msNavigationServiceProvider) {

        msNavigationServiceProvider.saveItem('usuarios', {
            title: 'Usuarios',
            icon: 'icon-account-multiple',
            state: 'app.usuarios',
            bodyClass: 'super-usuario',
            rol: 'SUPERUSUARIO'
        });

        msNavigationServiceProvider.saveItem('establecimientos', {
            title: 'Establecimientos',
            icon: 'icon-store',
            state: 'app.establecimientos',
            bodyClass: 'super-usuario',
            rol: 'SUPERUSUARIO'
        });

        msNavigationServiceProvider.saveItem('sedes', {
            title: 'Sedes',
            icon: 'icon-map-marker-radius',
            state: 'app.sedes',
            bodyClass: 'super-usuario',
            rol: 'SUPERUSUARIO'
        });
    }
})();