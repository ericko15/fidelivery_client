(function () {
  'use strict'

  angular
    .module('app.super-usuario.sedes')
    .controller('SedesController', SedesController);

  /** @ngInject */
  function SedesController(Restangular, $mdDialog, $mdToast, $http) {
    var vm = this
    // Varibales Globales
    vm.sedes = []
    vm.dtInstance = {};
    vm.dtOptions = {
      columnDefs: [{
        responsivePriority: 1,
        filterable: false,
        sortable: false
      }],
      autoWidth: false,
      language: {
        sEmptyTable: "No hay registro de sedes",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
        sInfoFiltered: "(filtrado desde _MAX_ registrsos)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLengthMenu: "Mostrar _MENU_ registros",
        sLoadingRecords: "Cargando...",
        sProcessing: "Procesando...",
        sSearch: "Buscar:",
        sZeroRecords: "No se encontraron registros que coincidar con la busqueda",
        oPaginate: {
          sFirst: "Primero",
          sLast: "Último",
          sNext: "Siguiente",
          sPrevious: "Anterior"
        },
        oAria: {
          sSortAscending: ": activar para ordenar las columnas ascendente",
          sSortDescending: ": activar para ordenar las columnas descendente"
        }
      },
      pagingType: 'simple',
      lengthMenu: [10, 20, 30, 50, 100],
      pageLength: 10,
      responsive: {
        details: {
          type: 'column',
          target: 1
        }
      }
    }
    // Funciones Globales
    vm.showDialogNuevaSede = showDialogNuevaSede
    vm.showDialogEditarSede = showDialogEditarSede
    vm.showDialogEliminarSede = showDialogEliminarSede

    function init() {
      $http.get('app/main/super-usuario/sedes/colombia.json').then(function (response) {
        vm.departamentos = response.data
      })
      Restangular.all('headquarters')
        .getList()
        .then(function (response) {
          vm.sedes = response
        })
    }

    function showDialogNuevaSede(ev) {
      $mdDialog.show({
        controller: nuevaSedeCtrl,
        templateUrl: 'app/main/super-usuario/sedes/dialog-nuevo-sede-template.html',
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          sede: null,
          departamentos: vm.departamentos
        }
      }).then(function (data) {
        if (data) {
          message('sede creada')
          init()
        }
      })
    }

    function showDialogEliminarSede(ev, sede) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, sede) {
          $scope.closeDialog = closeDialog
          $scope.remove = function remove() {
            Restangular.one('establishments', sede.establishment.id)
              .one('headquarters', sede.id)
              .remove($scope.sede)
              .then(function (response) {
                $mdDialog.hide(response)
              })
          }

          function closeDialog() {
            $mdDialog.hide()
          }
        },
        templateUrl: 'app/main/super-usuario/sedes/dialog-eliminar-sede-template.html',
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          sede: sede
        }
      }).then(function (data) {
        if (data) {
          message('sede eliminada')
          init()
        }
      })
    }

    function showDialogEditarSede(ev, sede) {
      sede = angular.copy(sede)
      $mdDialog.show({
        controller: nuevaSedeCtrl,
        templateUrl: 'app/main/super-usuario/sedes/dialog-editar-sede-template.html',
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          sede: sede,
          departamentos: vm.departamentos
        }
      }).then(function (data) {
        if (data) {
          message('sede Actualizada')
          init()
        }
      })
    }

    function nuevaSedeCtrl($scope, $mdDialog, Restangular, sede, departamentos) {
      $scope.establecimientos = JSON.parse(localStorage.getItem('establecimientos'))
      $scope.crear = crear
      $scope.editar = editar
      $scope.closeDialog = closeDialog
      $scope.departamentos = departamentos
      if (sede != null) {
        $scope.sede = sede
        $scope.establecimiento = sede.establishment.id
      } else {
        $scope.sede = {}
      }

      $scope.$watch('sede.departament', function (newValue, oldValue) {
        $scope.ciudades = []
        $scope.ciudades = $scope.departamentos.find(function (departamento) {
          if (departamento.departamento == $scope.sede.departament) {
            return departamento
          }
        })
        if ($scope.ciudades) {
          $scope.ciudades = $scope.ciudades.ciudades
        }
      })

      function crear() {
        Restangular.one('establishments', $scope.establecimiento)
          .all('headquarters')
          .post($scope.sede)
          .then(function (response) {
            $mdDialog.hide(response)
          })
      }

      function editar() {
        Restangular.one('establishments', $scope.establecimiento)
          .one('headquarters', sede.id)
          .patch($scope.sede)
          .then(function (response) {
            $mdDialog.hide(response)
          })
      }

      function closeDialog() {
        $mdDialog.hide()
      }
    }

    /**
     * @description Funcion para mostrar un mensaje en pantalla
     * @param {String} body 
     * @returns {void}
     */
    function message(body) {
      $mdToast.show({
        template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + body + '</div></md-toast>',
        hideDelay: 3000,
        position: 'bottom left',
        parent: '#content'
      })
    }

    init()

  }
})();
