(function () {
    'use strict';
  
    angular.module('app.super-usuario.sedes', []).config(config);
  
    /** @ngInject */
    function config($stateProvider) {
      $stateProvider
        .state('app.sedes', {
          url: '/sedes',
          views: {
            'content@app': {
              templateUrl: 'app/main/super-usuario/sedes/sedes.html',
              controller: 'SedesController as vm'
            }
          },
          bodyClass: 'sede',
          resolve: {
            redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
            isSuperUser: _isSuperUser
          }
        })
  
    }
  })();
  