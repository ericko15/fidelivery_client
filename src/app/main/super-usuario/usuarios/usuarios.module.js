(function () {
    'use strict';
  
    angular.module('app.super-usuario.usuarios', []).config(config);
  
    /** @ngInject */
    function config($stateProvider) {
      $stateProvider
        .state('app.usuarios', {
          url: '/usuarios',
          views: {
            'content@app': {
              templateUrl: 'app/main/super-usuario/usuarios/usuarios.html',
              controller: 'UsuariosController as vm'
            }
          },
          bodyClass: 'usuario',
          resolve: {
            redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
            isSuperUser: _isSuperUser
          }
        })
  
    }
  })();
  