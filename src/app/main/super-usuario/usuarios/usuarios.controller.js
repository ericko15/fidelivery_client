(function () {
  'use strict'

  angular
    .module('app.super-usuario.usuarios')
    .controller('UsuariosController', UsuariosController);

  /** @ngInject */
  function UsuariosController(Restangular, $mdDialog, $mdToast) {
    var vm = this
    // Varibales Globales
    vm.usuarios = []
    vm.dtInstance = {};
    vm.dtOptions = {
      columnDefs: [{
        responsivePriority: 1,
        filterable: false,
        sortable: false
      }],
      autoWidth: false,
      language: {
        sEmptyTable: "No hay registro de usuarios",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
        sInfoFiltered: "(filtrado desde _MAX_ registrsos)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLengthMenu: "Mostrar _MENU_ registros",
        sLoadingRecords: "Cargando...",
        sProcessing: "Procesando...",
        sSearch: "Buscar:",
        sZeroRecords: "No se encontraron registros que coincidar con la busqueda",
        oPaginate: {
          sFirst: "Primero",
          sLast: "Último",
          sNext: "Siguiente",
          sPrevious: "Anterior"
        },
        oAria: {
          sSortAscending: ": activar para ordenar las columnas ascendente",
          sSortDescending: ": activar para ordenar las columnas descendente"
        }
      },
      pagingType: 'simple',
      lengthMenu: [10, 20, 30, 50, 100],
      pageLength: 10,
      responsive: {
        details: {
          type: 'column',
          target: 1
        }
      }
    }
    // Funciones Globales
    vm.showDialogNuevoUsuario = showDialogNuevoUsuario
    vm.showDialogEditarUsuario = showDialogEditarUsuario
    vm.showDialogEliminarUsuario = showDialogEliminarUsuario

    function init() {
      Restangular.all('users')
        .getList()
        .then(function (response) {
          vm.usuarios = response
        })
    }

    function showDialogNuevoUsuario(ev, usuario) {
      $mdDialog.show({
        controller: nuevoUsuarioCtrl,
        templateUrl: 'app/main/super-usuario/usuarios/dialog-nuevo-usuario-template.html',
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          usuario: usuario
        }
      }).then(function (data) {
        if (data) {
          message('Usuario creado')
          init()
        }
      })
    }

    function showDialogEliminarUsuario(ev, usuario) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, usuario) {
          $scope.closeDialog = closeDialog
          $scope.remove = function remove() {
            Restangular.one('users', usuario.id)
              .remove()
              .then(function (response) {
                $mdDialog.hide(response)
              })
          }

          function closeDialog() {
            $mdDialog.hide()
          }
        },
        templateUrl: 'app/main/super-usuario/usuarios/dialog-eliminar-usuario-template.html',
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          usuario: usuario
        }
      }).then(function (data) {
        if (data) {
          message('Usuario eliminado')
          init()
        }
      })
    }

    function showDialogEditarUsuario(ev, usuario) {
      usuario = angular.copy(usuario)
      usuario.phone = parseInt(usuario.phone)
      $mdDialog.show({
        controller: nuevoUsuarioCtrl,
        templateUrl: 'app/main/super-usuario/usuarios/dialog-editar-usuario-template.html',
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          usuario: usuario
        }
      }).then(function (data) {
        if (data) {
          message('Usuario Actualizado')
          init()
        }
      })
    }

    function nuevoUsuarioCtrl($scope, $mdDialog, Restangular, usuario) {
      $scope.establecimientos = JSON.parse(localStorage.getItem('establecimientos'))
      $scope.cargarSedes = cargarSedes
      $scope.crear = crear
      $scope.editar = editar
      $scope.closeDialog = closeDialog
      $scope.usuario = usuario

      function cargarSedes() {
        $scope.sedes = $scope.establecimientos.find(function (establecimiento) {
          if (establecimiento.id == $scope.vendedor.establecimiento) {
            return establecimiento
          }
        }).headquarters
      }

      function crear() {
        var consulta
        if ($scope.type == 1) {
          $scope.usuario.phone = $scope.usuario.phone.toString()
          $scope.usuario.document = $scope.vendedor.documento.toString()
          $scope.usuario.type = 1
          consulta = Restangular.one('establishments', $scope.vendedor.establecimiento)
            .one('headquarters', $scope.vendedor.sede)
            .all('employees')
        } else {
          $scope.usuario.type = $scope.administrador.type
          $scope.usuario.sms = $scope.administrador.sms
          consulta = Restangular.all('administrators')
        }
        consulta.post($scope.usuario)
          .then(function (response) {
            $mdDialog.hide(response)
          }).catch(function (err){
            message('error al crear el usuario')
          })
      }

      function editar() {
        Restangular.one('users', usuario.id)
          .patch($scope.usuario)
          .then(function (response) {
            $mdDialog.hide(response)
          })
      }

      function closeDialog() {
        $mdDialog.hide()
      }
    }

    /**
     * @description Funcion para mostrar un mensaje en pantalla
     * @param {String} body 
     * @returns {void}
     */
    function message(body) {
      $mdToast.show({
        template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + body + '</div></md-toast>',
        hideDelay: 3000,
        position: 'bottom left',
        parent: '#content'
      })
    }

    init()

  }
})();
