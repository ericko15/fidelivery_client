(function () {
    'use strict';
  
    angular.module('app.super-usuario.establecimientos', []).config(config);
  
    /** @ngInject */
    function config($stateProvider) {
      $stateProvider
        .state('app.establecimientos', {
          url: '/establecimientos',
          views: {
            'content@app': {
              templateUrl: 'app/main/super-usuario/establecimientos/establecimientos.html',
              controller: 'EstablecimientosController as vm'
            }
          },
          bodyClass: 'establecimiento',
          resolve: {
            redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
            isSuperUser: _isSuperUser
          }
        })
  
    }
  })();
  