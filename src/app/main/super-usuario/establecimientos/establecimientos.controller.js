(function () {
  'use strict'

  angular
    .module('app.super-usuario.establecimientos')
    .controller('EstablecimientosController', EstablecimientosController);

  /** @ngInject */
  function EstablecimientosController(Restangular, $mdDialog, $mdToast) {
    var vm = this
    // Varibales Globales
    vm.establecimientos = []
    vm.dtInstance = {};
    vm.dtOptions = {
      columnDefs: [{
        responsivePriority: 1,
        filterable: false,
        sortable: false
      }],
      autoWidth: false,
      language: {
        sEmptyTable: "No hay registro de establecimientos",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
        sInfoFiltered: "(filtrado desde _MAX_ registrsos)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLengthMenu: "Mostrar _MENU_ registros",
        sLoadingRecords: "Cargando...",
        sProcessing: "Procesando...",
        sSearch: "Buscar:",
        sZeroRecords: "No se encontraron registros que coincidar con la busqueda",
        oPaginate: {
          sFirst: "Primero",
          sLast: "Último",
          sNext: "Siguiente",
          sPrevious: "Anterior"
        },
        oAria: {
          sSortAscending: ": activar para ordenar las columnas ascendente",
          sSortDescending: ": activar para ordenar las columnas descendente"
        }
      },
      pagingType: 'simple',
      lengthMenu: [10, 20, 30, 50, 100],
      pageLength: 10,
      responsive: {
        details: {
          type: 'column',
          target: 1
        }
      }
    }
    // Funciones Globales
    vm.showDialogNuevoEstablecimiento = showDialogNuevoEstablecimiento
    vm.showDialogEditarEstablecimiento = showDialogEditarEstablecimiento
    vm.showDialogEliminarEstablecimiento = showDialogEliminarEstablecimiento

    function init() {
      Restangular.all('establishments')
        .getList()
        .then(function (response) {
          vm.establecimientos = response
        })
    }

    function showDialogNuevoEstablecimiento(ev) {
      $mdDialog.show({
        controller: nuevoEstablecimientoCtrl,
        templateUrl: 'app/main/super-usuario/establecimientos/dialog-nuevo-establecimiento-template.html',
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          establecimiento: null
        }
      }).then(function (data) {
        if (data) {
          message('establecimiento creado')
          init()
        }
      })
    }

    function showDialogEliminarEstablecimiento(ev, establecimiento) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, establecimiento) {
          $scope.closeDialog = closeDialog
          $scope.remove = function remove() {
            console.log(establecimiento)
            Restangular.one('administrators', establecimiento.administrator_id)
              .one('establishments', establecimiento.id)
              .remove()
              .then(function (response) {
                $mdDialog.hide(response)
              })
          }

          function closeDialog() {
            $mdDialog.hide()
          }
        },
        templateUrl: 'app/main/super-usuario/establecimientos/dialog-eliminar-establecimiento-template.html',
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          establecimiento: establecimiento
        }
      }).then(function (data) {
        if (data) {
          message('Establecimiento eliminado')
          init()
        }
      })
    }

    function showDialogEditarEstablecimiento(ev, establecimiento) {
      establecimiento = angular.copy(establecimiento)
      $mdDialog.show({
        controller: nuevoEstablecimientoCtrl,
        templateUrl: 'app/main/super-usuario/establecimientos/dialog-editar-establecimiento-template.html',
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          establecimiento: establecimiento
        }
      }).then(function (data) {
        if (data) {
          message('Establecimiento Actualizado')
          init()
        }
      })
    }

    function nuevoEstablecimientoCtrl($scope, $mdDialog, Restangular, establecimiento) {
      $scope.administradores = JSON.parse(localStorage.getItem('administradores'))
      $scope.crear = crear
      $scope.editar = editar
      $scope.closeDialog = closeDialog
      if (establecimiento != null) {
        $scope.establecimiento = establecimiento
        $scope.administrador = establecimiento.administrator.id
      }

      function crear() {
        Restangular.one('administrators', $scope.administrador)
          .all('establishments')
          .post($scope.establecimiento)
          .then(function (response) {
            $mdDialog.hide(response)
          })
      }

      function editar() {
        Restangular.one('administrators', $scope.administrador)
          .one('establishments', establecimiento.id)
          .patch($scope.establecimiento)
          .then(function (response) {
            $mdDialog.hide(response)
          })
      }

      function closeDialog() {
        $mdDialog.hide()
      }
    }

    /**
     * @description Funcion para mostrar un mensaje en pantalla
     * @param {String} body 
     * @returns {void}
     */
    function message(body) {
      $mdToast.show({
        template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + body + '</div></md-toast>',
        hideDelay: 3000,
        position: 'bottom left',
        parent: '#content'
      })
    }

    init()

  }
})();
