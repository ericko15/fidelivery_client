/*$(document).on("focus mouseover", ".tooltip", function () {
    $('.tooltip').tooltipster();
});*/

'use strict'

if (!String.format) {
  /**
   * Formatea un String como en C# o VB.NET: String.format(Hola {0}, 'Andres') = 'Hola Andres'
   * @param {array} format
   * @returns {string}
   */
  String.format = function (format) {
    var args = Array.prototype.slice.call(arguments, 1)
    return format.replace(/{(\d+)}/g, function (match, number) {
      return typeof args[number] !== 'undefined' ? args[number] : match
    })
  }
}

/**
 * Capitaliza una cadena: Convierte en mayúscula la primera letra de cada palabra.
 * @returns {string}
 */
String.prototype.capitalize = function () {
  return this.replace(/(?:^|\s)\S/g, function (a) {
    return a.toUpperCase();
  });
};

/**
 * Pads right (rellena hacia la derecha con el caracter deseado),
 * hasta la cantidad de elementos.
 * http://sajjadhossain.com/2008/10/31/javascript-string-trimming-and-padding/
 */
String.prototype.rpad = function (padString, length) {
  var str = this;
  while (str.length < length)
    str = str + padString;
  return str;
};

/**
 * Convierte una hora en formato de 24 horas, a una en formato de 12 horas.
 * Por ejemplo: 17 = 5 pm
 *
 * @param {number} hours Las horas en formato de 24 horas.
 * @returns {string}
 */
function convertirFormato24HorasA12(horas24) {
  var suffix = horas24 >= 12 ? "pm" : "am";
  var horas12 = ((horas24 + 11) % 12 + 1) + ' ' + suffix;
  return horas12;
}

/**
 * Convierte una fecha en el formato YYYY-MM-DD.
 *
 * @param {Date} fecha
 * @returns String
 */
function fechaYYYYMMDD(fecha) {
  var anio = fecha.getFullYear();
  var mes = fecha.getMonth() + 1;
  var dia = fecha.getDate();
  var ymd = anio.toString() + '-';
  ymd += mes < 10 ? '0' + mes : mes;
  ymd += '-';
  ymd += dia < 10 ? '0' + dia : dia;
  return ymd;
}

/**
 * Retorna el día de la semana (en letras y en español) correspondiente a una fecha. (Beta)
 *
 * @param fecha_recibida La fecha a evaluar.
 * @returns El día de la semana correspondiente a la fecha recibida.
 */
function getDiaSemana(fecha_recibida) {
  var dias = ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'];
  return dias[fecha_recibida.getDay()];
}


/**
 * Retorna el objeto seleccionado dentro de un vector,
 * conociendo el id del objeto seleccionado.
 * @param {[]} array El vector a recorrer.
 * @param {number|string} id El valor del identificador del objeto seleccionado.
 * @param {string} [paramId] El nombre del parámetro identificador.
 * @returns {object}
 */
function getObjetoEnVectorPorId(array, id, paramId) {
  paramId = paramId || 'id';
  return array[array.findIndex(function (element) {
    return element[paramId] == id;
  })];
}

/**
 * Retorna un array con los valores de un objeto y no sus claves/keys.
 * @param {Object} object El objeto a recorrer.
 * @returns {Array}
 */
function getObjectValues(object) {
  var values = [];
  for (var key in object) {
    values.push(object[key]);
  }
  return values;
}

/**
 * Retorna un array con los valores de una propiedad que los objetos del array tengan.
 * @param {Object[]} array El array para recorrer.
 * @param {string} property La propiedad que se necesita para extraer los valores del array.
 * @param {string} [propertyCondition] La propiedad para determinar la inclusión del objeto.
 * @param {string} [inclusionValue] El valor para determinar la inclusión del objeto.
 * @returns {Array}
 */
function getPropertyInArrayObject(array, property, propertyCondition, inclusionValue) {
  var values = [];
  array = (!array) ? [] : array;
  array.forEach(function (element) {
    if (element.hasOwnProperty(property)) {
      if (propertyCondition && inclusionValue) {
        if (element.hasOwnProperty(propertyCondition)) {
          if (element[propertyCondition] == inclusionValue) {
            values.push(element[property]);
          }
        }
      } else {
        values.push(element[property]);
      }
    }
  }, this);
  return values;
}

/**
 * Función para validar si una tecla presionada es numérica o no.
 * @param {event} evt
 * @returns {boolean}
 */
function isNumberKey(evt) {
  var event = (!event) ? {} : event;
  var charCode = (evt.which) ? evt.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;
}

/**
 * Retorna una cadena sin tildes ni eñes.
 * @param {String} cadena
 * @returns {String}
 */
var normalize = (function () {
  var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç",
    to = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
    mapping = {};

  for (var i = 0, j = from.length; i < j; i++)
    mapping[from.charAt(i)] = to.charAt(i);

  return function (str) {
    var ret = [];
    for (var i = 0, j = str.length; i < j; i++) {
      var c = str.charAt(i);
      if (mapping.hasOwnProperty(str.charAt(i)))
        ret.push(mapping[c]);
      else
        ret.push(c);
    }
    return ret.join('');
  }
})();

/** Function that count occurrences of a substring in a string;
 * @param {String} string               The string
 * @param {String} subString            The sub string to search for
 * @param {Boolean} [allowOverlapping]  Optional. (Default:false)
 * @author Vitim.us http://stackoverflow.com/questions/4009756/how-to-count-string-occurrence-in-string/7924240#7924240
 */
function occurrences(string, subString, allowOverlapping) {

  string += "";
  subString += "";
  if (subString.length <= 0) return (string.length + 1);

  var n = 0,
    pos = 0,
    step = allowOverlapping ? 1 : subString.length;

  while (true) {
    pos = string.indexOf(subString, pos);
    if (pos >= 0) {
      ++n;
      pos += step;
    } else break;
  }
  return n;
}

/**
 * Retorna un array de enteros que estaban formateados como string.
 * Ejemplo: ['1', '2'] => [1, 2]
 * Si un elemento es null, retorna 0.
 * @param {array} array
 * @param {number} base
 * @returns {number}
 */
function parseIntArray(array, base) {
  base = (!base) ? 10 : base;
  return array.map(function (x) { return parseInt(x || 0, base); });
}

/**
 * Función range(), similar a la de Python. Retorna un array, recibiendo su inicio, final (no incluyente) y paso.
 *
 * @param start Inicio
 * @param stop Final (no incluyente)
 * @param step Paso
 * @returns array
 */
function range(start, stop, step) {
  if (typeof stop == 'undefined') {
    // one param defined
    stop = start;
    start = 0;
  }

  if (typeof step == 'undefined') {
    step = 1;
  }

  if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
    return [];
  }

  var result = [];
  for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
    result.push(i);
  }

  return result;
};

/**
 * Suma los elementos de un array de valores, no de objetos.
 * @param {array} array El array para sumar sus elementos.
 * @returns {number}
 */
function sumarElementosArray(array) {
  array = (!array) ? [] : array;
  return array.reduce(function (previousValue, currentValue) {
    return Number(currentValue) + Number(previousValue);
  }, 0);
}

function formato_fecha(fecha) {
  var anio = fecha.getFullYear();
  var mes = fecha.getMonth() + 1;
  var dia = fecha.getDate();
  var ymd = anio.toString() + '-';
  ymd += mes < 10 ? '0' + mes : mes;
  ymd += '-';
  ymd += dia < 10 ? '0' + dia : dia;
  return ymd;
}

/**
 * 
 * @param {Object} $location  
 * @param {Object} $auth
 * @description Esta funcion redirecciona al login si el usuario no esta authenticado
 */
function _redirectIfNotAuthenticated($location, $auth) {
  if (!$auth.isAuthenticated()) {
    $location.path('pages/auth/login')
  }
}

/**
 * 
 * @param {Object} $location  
 * @param {Object} $auth
 * @description Esta funcion redirecciona  si el usuario no esta authenticado
 */
function _skipIfAuthenticated($location, $auth) {
  if ($auth.isAuthenticated()) {
    var rol = $auth.getPayload().user.rol
    if (rol === 1) {
      $location.path('usuarios')
    } else if (rol === 2) {
      $location.path('historial-pedidos')
    } else if (rol === 3) {
      $location.path('pedidos')
    }
  }
}

/**
 * 
 * @param {Object} $location  
 * @param {Object} $auth
 * @description Esta funcion redirecciona si el usuario no es admin
 */
function _isSuperUser($location, $auth) {
  var rol = $auth.getPayload().user.rol
  if (rol !== 1) {
    if (rol === 3) {
      $location.path('pedidos')
    } else if(rol === 2) {
      $location.path('historial-pedidos')
    }
  }
}

/**
 * 
 * @param {Object} $location  
 * @param {Object} $auth
 * @description Esta funcion redirecciona si el usuario no es admin
 */
function _isAdmin($location, $auth) {
  var rol = $auth.getPayload().user.rol
  if (rol !== 2) {
    if (rol === 1) {
      $location.path('usuarios')
    } else if (rol === 3) {
      $location.path('pedidos')
    }
  }
}

/**
 * 
 * @param {Object} $location  
 * @param {Object} $auth
 * @description Esta funcion redirecciona si el usuario no es vendedor
 */
function _isVendedor($location, $auth) {
  var rol = $auth.getPayload().user.rol
  if (rol !== 3) {
    if (rol === 1) {
      $location.path('usuarios')
    } else if (rol === 2) {
      $location.path('historial-pedidos')
    }
  }
}