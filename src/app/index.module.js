(function () {
  'use strict';
  /**
   * Main module of the Fuse
   */
  angular.module('fuse', [
    // Core
    'app.core',
    'satellizer',
    'angular-jwt',
    'restangular',
    // Navigation
    'app.navigation',
    // Toolbar
    'app.toolbar',
    'app.pages',
    'app.super-usuario',
    'app.vendedor',
    'app.administrador',
    'datatables',
    'chart.js',
    'angucomplete-alt'
  ]);
})();
